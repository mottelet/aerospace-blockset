// This file is released under the 3-clause BSD license. See COPYING-BSD.
//=================================

// Test flags
//<-- XCOS TEST -->
//<-- NO CHECK ERROR OUTPUT -->
//<-- TEST WITH GRAPHIC -->
//<-- NO CHECK REF -->

s = filesep();
[macros,base_path] = libraryinfo('xcos_aerolib');
demos_path = base_path + '..'+ s + 'demos';

// Define number of demos\
demos_count = 8;

// Locate demo files
demo_files = findfiles(demos_path, '*.zcos');
number_of_files = size(demo_files, 1);
[flag,errmsg] = assert_checktrue(number_of_files==demos_count);
if(~flag) 
    assert_generror("Number of demo diagrams is equal to " + string(number_of_files) + " instead of " + string(demos_count))
end
demo_files = gsort(demo_files);

for i=1:demos_count
    // Check if demos can be imported
    result = importXcosDiagram(demos_path + s + demo_files(i));
    if(~result) 
        assert_generror("Failed to load demo: " + demo_files(i))
    else
        try
            scicos_simulate(scs_m);
        catch
            assert_generror("Demo " + demo_files(i) + " failed")
        end
    end
end

// Locate macro files
macro_files = findfiles(demos_path, '*.sce');
number_of_macro = size(macro_files,1);
[flag,errmsg] = assert_checktrue(number_of_macro==demos_count+1);
if(~flag) 
    assert_generror("Number of demo launcher scripts is equal to " + string(number_of_launchers) + " instead of " + string(demos_count))
end
macro_files = gsort(macro_files);


// create list of launcher files
launcher_files = [];
for i =1:number_of_macro
	if(strcmp(macro_files(i), "aerospace_blockset.dem.gateway.sce"));
		launcher_files= [launcher_files; macro_files(i)];
	end
end
number_of_launchers = size(launcher_files, 1);
assert_checktrue(number_of_launchers==demos_count)


// Check if all of the demos have proper launchers
demo_files = strsubst(demo_files,'.zcos','');
launcher_files = strsubst(launcher_files,'.sce','');
assert_checktrue(and(demo_files == launcher_files));
