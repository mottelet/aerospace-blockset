// This file is released under the 3-clause BSD license. See COPYING-BSD.
//=================================


// Test flags
//<-- XCOS TEST -->
//<-- NO CHECK ERROR OUTPUT -->

//Load xcos test diagram for quat_to_euler tests
[macros,path] = libraryinfo('xcos_aerolib');
result = importXcosDiagram(path + "../tests/unit_tests" +"/axa_to_quat.zcos");
assert_checktrue(result);

//This diagram uses 3 variables : 
//  axis_in : Euler axis (vector)
//  angle_in : Euler angle
//  quat_out :quaternion output (vector)


axis_in = [ 0.57735026918962573    0.37139067635410367;  
             0.57735026918962573    0.55708601453115558;  
             0.57735026918962573    0.74278135270820744];
angle_in = [ 1 2];
quat_exp = [ 0.87758256189037276    0.54030230586813977;  
             0.27679646376951794    0.3125144781801584;   
             0.27679646376951794    0.46877171727023764;  
             0.27679646376951794    0.6250289563603169];  
 
// Define expected values
exp_time = [0; 1; 2];
            
scicos_simulate(scs_m);

//Validate results
assert_checkequal(exp_time, quat_out.time);
assert_checkalmostequal(quat_exp, quat_out.values(:,:,1), %eps);
assert_checkalmostequal(quat_exp, quat_out.values(:,:,2), %eps);
assert_checkalmostequal(quat_exp, quat_out.values(:,:,3), %eps);
