// This file is released under the 3-clause BSD license. See COPYING-BSD.
//=================================


// Test flags
//<-- XCOS TEST -->
//<-- NO CHECK ERROR OUTPUT -->

//Load xcos test diagram for vector_angle tests
[macros,path] = libraryinfo('xcos_aerolib');
result = importXcosDiagram(path + "../tests/unit_tests" +"/dot_product.zcos");
assert_checktrue(result);

//This diagram uses 3 variables : 
//  vectors1_in : input vectors (from_workspace)
//  vectors2_in : input vectors (from_workspace)
//  product_out : output product (to_workspace)

vectors1_in = [	0,     1,   1.2,   1;
		0,    -1,   0,     2;
		0,     1,   0,     3];

vectors2_in = [	7.1,   1,   0,    -1;
		7.2,  -1,   1.2,  -1;
		7.3,   1,   0,    -1];

scicos_simulate(scs_m);

// Define expected values
exp_time  = [0; 1; 2];
exp_value = [0, 3, 0, -6]; 

//Validate results
assert_checkequal(product_out.time, exp_time);
assert_checkalmostequal(product_out.values(:,:,1), exp_value, 1e-4);
assert_checkalmostequal(product_out.values(:,:,2), exp_value, 1e-4);
assert_checkalmostequal(product_out.values(:,:,3), exp_value, 1e-4);
