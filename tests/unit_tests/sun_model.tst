// This file is released under the 3-clause BSD license. See COPYING-BSD.
//=================================

// Test flags
//<-- XCOS TEST -->
//<-- NO CHECK ERROR OUTPUT -->

//Load xcos test diagram for sun_model tests
[macros,path] = libraryinfo('xcos_aerolib');
result = importXcosDiagram(path + "../tests/unit_tests" + "/sun_model.xcos");
assert_checktrue(result);

//This diagram uses 2 variables : 
//  cjd_in - starting cjd time
//  position_out - position calculated from the diagram

scicos_simulate(scs_m);

// Define expected values
time_expected = [0; 1; 2];
position_expected = 10^10 * [ 	2.5136212,  - 13.298529,  - 5.7639;     
				2.7709993,  - 13.255301,  - 5.7449522;  
				3.0275149,  - 13.207947,  - 5.7242162 ];

//Validate results
assert_checkequal(position_out.time, time_expected);
assert_checkalmostequal(position_out.values, position_expected, 0.0000001);
