// This file is released under the 3-clause BSD license. See COPYING-BSD.
//=================================

// Test flags
//<-- XCOS TEST -->
//<-- NO CHECK ERROR OUTPUT -->

//Load xcos test diagram for sun_model tests
[macros,path] = libraryinfo('xcos_aerolib');
result = importXcosDiagram(path + "../tests/unit_tests" + "/planet_model.zcos");
assert_checktrue(result);

//This diagram uses 3 variables : 
//  cjd_in - starting cjd time
//  position_out - position calculated from the diagram
//  velocity_out - velocity calculated from the diagram

scicos_simulate(scs_m);

// Define expected values
time_expected = [0; 1; 2];
position_expected = 10^10 * [	-2.5157025,    23.558243,    0.5546121;  
				-2.7159257,    23.552896,    0.5593973;  
				-2.915947,     23.545799,    0.5641412 ];

velocity_expected = [	-23185.12,   -517.35717,    556.21945; 
			-23162.578,  -720.19842,    551.45576;  
			-23138.363,  -922.55065,    546.6653  ];

//Validate results
assert_checkequal(position_out.time, time_expected);
assert_checkalmostequal(position_out.values, position_expected, 0.0000001);
assert_checkalmostequal(velocity_out.values, velocity_expected, 0.0000001);
