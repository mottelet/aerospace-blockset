// This file is released under the 3-clause BSD license. See COPYING-BSD.
//=================================

// Test flags
//<-- XCOS TEST -->
//<-- NO CHECK ERROR OUTPUT -->

//Load xcos test diagram for moon_model tests
[macros,path] = libraryinfo('xcos_aerolib');
result = importXcosDiagram(path + "../tests/unit_tests" + "/moon_model.xcos");
assert_checktrue(result);

//This diagram uses 2 variables : 
//  cjd_in - starting cjd time
//  position_out - position calculated from the diagram

scicos_simulate(scs_m);

// Define expected values
time_expected = [0; 1; 2];
position_expected = [	- 2.475D+08,    2.757D+08,    87821897.;  
			- 3.056D+08,    2.145D+08,    57617729.; 
			- 3.468D+08,    1.417D+08,    24297426. ];

//Validate results
assert_checkequal(position_out.time, time_expected);
assert_checkalmostequal(position_out.values, position_expected, 1e-3);
