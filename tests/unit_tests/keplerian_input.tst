// This file is released under the 3-clause BSD license. See COPYING-BSD.
//=================================


// Test flags
//<-- XCOS TEST -->
//<-- NO CHECK ERROR OUTPUT -->

//Load xcos test diagram for rad2deg tests
[macros,path] = libraryinfo('xcos_aerolib');
result = importXcosDiagram(path + "../tests/unit_tests" + "/keplerian_input.xcos");
assert_checktrue(result);

//This diagram uses 12 variables : 
//    dga_in - Semi-major axis [m] (input value)
//    ecc_in - Eccentricity (input value)
//    inc_in - Inclination [rad] (input value)
//    pom_in - Periapsis argument [rad] (input value)
//    gom_in - Right ascension of ascending node [rad] (input value)
//    anm_in - Mean anomaly [rad] (input value)
//    dga_out - Semi-major axis [m] (output value)
//    ecc_out - Eccentricity (output value)
//    inc_out - Inclination [rad] (output value)
//    pom_out - Periapsis argument [rad] (output value)
//    gom_out - Right ascension of ascending node [rad] (output value)
//    anm_out - Mean anomaly [rad] (output value)


// Define input values
dga_in = 9150600;
ecc_in = 0.1;
inc_in = 26.126875;
pom_in = 19.767044;
gom_in = 76.776345;
anm_in = 56.550934;

//Define expected values
dga_exp = 9150600;
ecc_exp = 0.1;
inc_exp = 0.456;
pom_exp = 0.345;
gom_exp = 1.34;
anm_exp = 0.987;

scicos_simulate(scs_m);
values = [dga_out.values; ecc_out.values; inc_out.values; pom_out.values; gom_out.values; anm_out.values];
time = [dga_out.time; ecc_out.time; inc_out.time; pom_out.time; gom_out.time; anm_out.time];

// Define expected values
expected = [dga_exp; ecc_exp; inc_exp; pom_exp; gom_exp; anm_exp];
exp_time = [0.1; 0.1; 0.1; 0.1; 0.1; 0.1];

//Validate results
assert_checkalmostequal(values, expected,1e-6);
assert_checkequal(time, exp_time);
