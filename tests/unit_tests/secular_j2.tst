// This file is released under the 3-clause BSD license. See COPYING-BSD.
//=================================

// Test flags
//<-- XCOS TEST -->
//<-- NO CHECK ERROR OUTPUT -->

//Load xcos test diagram for rad2deg tests
[macros,path] = libraryinfo('xcos_aerolib');
result = importXcosDiagram(path + "../tests/unit_tests" + "/secular_j2.xcos");
assert_checktrue(result);

//This diagram uses 12 variables : 
//    dga_in - Semi-major axis [m] (input value)
//    ecc_in - Eccentricity (input value)
//    inc_in - Inclination [rad] (input value)
//    pom_in - Periapsis argument [rad] (input value)
//    gom_in - Right ascension of ascending node [rad] (input value)
//    anm_in - Mean anomaly [rad] (input value)
//    dga_out - Semi-major axis [m] (output value)
//    ecc_out - Eccentricity (output value)
//    inc_out - Inclination [rad] (output value)
//    pom_out - Periapsis argument [rad] (output value)
//    gom_out - Right ascension of ascending node [rad] (output value)
//    anm_out - Mean anomaly [rad] (output value)


// Define input values
dga_in = 7070.e3;
ecc_in = 0.001;
inc_in = 1.7104227;
pom_in = 0;
gom_in = 0;
anm_in = 0;

// Define Expected values
dga_exp = [7070000.; 7070000.; 7070000.; 7070000.; 7070000.; 7070000.; 7070000.; 7070000.];
ecc_exp = [0.001;0.001;0.001;0.001;0.001;0.001;0.001;0.001];
inc_exp = [1.7104227; 1.7104227; 1.7104227; 1.7104227; 1.7104227; 1.7104227; 1.7104227; 1.7104227];
pom_exp = [0; -0.0003803; -0.0007606; -0.0011409; -0.0015212; -0.0019016; -0.0022819; -0.0026622];
gom_exp = [0; 0.0001172; 0.0002344; 0.0003516; 0.0004688; 0.0005860; 0.0007033; 0.0008205];
anm_exp = [0; 0.6368217; 1.2736433; 1.910465; 2.5472867; 3.1841084; 3.82093; 4.4577517];
time_exp = [0; 0.0069444; 0.0138888; 0.0208332; 0.0277776; 0.034722; 0.0416664; 0.0486108];

// Simulate
scicos_simulate(scs_m);

//Validate results
assert_checkequal(dga_out.values, dga_exp);
assert_checkequal(ecc_out.values, ecc_exp);
assert_checkequal(inc_out.values, inc_exp);
assert_checkalmostequal(pom_out.values, pom_exp, 1e-4);
assert_checkalmostequal(gom_out.values, gom_exp, 1e-4);
assert_checkalmostequal(anm_out.values, anm_exp, 1e-4);
assert_checkalmostequal(dga_out.time, time_exp, 1e-4);

//clear result dga_in ecc_in inc_in pom_in gom_in anm_in dga_exp ecc_exp inc_exp pom_exp gom_exp anm_exp time_exp dga_out ecc_out inc_out pom_out gom_out anm_out scs_m;
