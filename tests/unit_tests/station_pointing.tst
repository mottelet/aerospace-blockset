// This file is released under the 3-clause BSD license. See COPYING-BSD.
//=================================


// Test flags
//<-- XCOS TEST -->
//<-- NO CHECK ERROR OUTPUT -->

//Load xcos test diagram for cross_product tests
[macros,path] = libraryinfo('xcos_aerolib');
result = importXcosDiagram(path + "../tests/unit_tests" +"/station_pointing.zcos");
assert_checktrue(result);

//This diagram uses 3 variables : 
//  station_in : parameters of the ground station (from_workspace)
//  position_in : position of the satellite in ecf frame (from_workspace)
//  AzEl_out : Azimut and elevation of the satelite over the ground station (to_workspace)
//  distance_out : orange from ground station to the satelite (to_workspace)

station_in = [%pi/3; %pi/2; 95; %pi*3/180];
position_in = [10000;10000;10000];

scicos_simulate(scs_m);

// Define expected values
exp_time  = [0.1; 1.1; 2.1];
exp_AzEl = [    0.5918720587873985,  -1.56797303011478428;  
                0.5918720587873985,  -1.56797303011478428;  
                0.5918720587873985,  -1.56797303011478428  ];
exp_distance =[ 6348564.57431035582;
                6348564.57431035582;  
                6348564.57431035582 ];

//Validate results
assert_checkequal(AzEl_out.time, exp_time);
assert_checkequal(distance_out.time, exp_time);
assert_checkalmostequal(AzEl_out.values, exp_AzEl, 1e-6);
assert_checkalmostequal(distance_out.values, exp_distance, 1e-6);
