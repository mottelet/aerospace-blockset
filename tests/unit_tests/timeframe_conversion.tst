// This file is released under the 3-clause BSD license. See COPYING-BSD.
//=================================


// Test flags
//<-- XCOS TEST -->
//<-- NO CHECK ERROR OUTPUT -->

//Load xcos test diagram for rad2deg tests
[macros,path] = libraryinfo('xcos_aerolib');
result = importXcosDiagram(path + "../tests/unit_tests" +"/timeframe_conversion.xcos");
assert_checktrue(result);

//This diagram uses 6 variables 
cjd_cal_in = 22646.128;  //3:4,5 on 2nd january 2012
cjd_mjd_in = 22999.999;
cjd_jd_in = 22047.29;
jd_mjd_in = 2455928.6;  //3:4,5 on 2nd january 2012
jd_cal_in = 245456.9;
mjd_cal_in = 55928.128;  //3:4,5 on 2nd january 2012

rad_angle = [0, -%pi; 2 * %pi, -4 * %pi];
scicos_simulate(scs_m);

// Define expected values
exp_time = [0.1; 1.1; 2.1];

//Validate results
assert_checkequal(exp_time, cjd_cal_out.time);
assert_checkequal(exp_time, cjd_mjd_out.time);
assert_checkequal(exp_time, cjd_jd_out.time);
assert_checkequal(exp_time, jd_mjd_out.time);
assert_checkequal(exp_time, jd_cal_out.time);
assert_checkequal(exp_time, mjd_cal_out.time);

assert_checkalmostequal(cjd_cal_out.values(1), cjd_cal_in, 1e-13);
assert_checkalmostequal(cjd_cal_out.values(2), cjd_cal_in, 1e-13);
assert_checkalmostequal(cjd_cal_out.values(3), cjd_cal_in, 1e-13);

assert_checkalmostequal(cjd_mjd_out.values(1), cjd_mjd_in, 1e-13);
assert_checkalmostequal(cjd_mjd_out.values(2), cjd_mjd_in, 1e-13);
assert_checkalmostequal(cjd_mjd_out.values(3), cjd_mjd_in, 1e-13);

assert_checkalmostequal(cjd_jd_out.values(1), cjd_jd_in, 1e-13);
assert_checkalmostequal(cjd_jd_out.values(2), cjd_jd_in, 1e-13);
assert_checkalmostequal(cjd_jd_out.values(3), cjd_jd_in, 1e-13);

assert_checkalmostequal(jd_mjd_out.values(1), jd_mjd_in, 1e-13);
assert_checkalmostequal(jd_mjd_out.values(2), jd_mjd_in, 1e-13);
assert_checkalmostequal(jd_mjd_out.values(3), jd_mjd_in, 1e-13);

assert_checkalmostequal(jd_cal_out.values(1), jd_cal_in, 1e-13);
assert_checkalmostequal(jd_cal_out.values(2), jd_cal_in, 1e-13);
assert_checkalmostequal(jd_cal_out.values(3), jd_cal_in, 1e-13);

assert_checkalmostequal(mjd_cal_out.values(1), mjd_cal_in, 1e-13);
assert_checkalmostequal(mjd_cal_out.values(2), mjd_cal_in, 1e-13);
assert_checkalmostequal(mjd_cal_out.values(3), mjd_cal_in, 1e-13);
