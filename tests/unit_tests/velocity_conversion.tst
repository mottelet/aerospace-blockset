// This file is released under the 3-clause BSD license. See COPYING-BSD.
//=================================


// Test flags
//<-- XCOS TEST -->
//<-- NO CHECK ERROR OUTPUT -->

//Load xcos test diagram for VELOCITY_CONVERSION tests
[macros,path] = libraryinfo('xcos_aerolib');
result = importXcosDiagram(path + "../tests/unit_tests" +"/km-h2m-s.zcos");
assert_checktrue(result);

//This diagram uses 2 variables : 
//  velocity_from
//  velocity_to

velocity_from = [0, 16; -10, 12.3];
scicos_simulate(scs_m);

// Define expected values
exp_time = [0.1; 1.1; 2.1];
exp_value = [0, 4.4444444; -2.7777778, 3.4166667];
exp_value = [0, 4.4444444; -2.7777778, 3.4166667];
exp_value = [0, 4.4444444; -2.7777778, 3.4166667];

//Validate results
assert_checkequal(exp_time, velocity_to.time);
assert_checkalmostequal(exp_value, velocity_to.values(:,:,1), 1e-6);
assert_checkalmostequal(exp_value, velocity_to.values(:,:,2), 1e-6);
assert_checkalmostequal(exp_value, velocity_to.values(:,:,3), 1e-6);


//Load xcos test diagram for VELOCITY_CONVERSION tests
result = importXcosDiagram(path + "../tests/unit_tests" +"/au-yr2km-mn.zcos");
assert_checktrue(result);

//This diagram uses 2 variables : 
//  velocity_from
//  velocity_to

velocity_from = [1, -0.3; 0, 123];
scicos_simulate(scs_m);

// Define expected values
exp_time = [0.1; 1.1; 2.1];
exp_value = [284.42823, -85.328468; 0, 34984.672];


//Validate results
assert_checkequal(exp_time, velocity_to.time);
assert_checkalmostequal(exp_value, velocity_to.values(:,:,1), 1e-6);
assert_checkalmostequal(exp_value, velocity_to.values(:,:,2), 1e-6);
assert_checkalmostequal(exp_value, velocity_to.values(:,:,3), 1e-6);
