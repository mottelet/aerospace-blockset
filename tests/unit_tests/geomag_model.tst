// This file is released under the 3-clause BSD license. See COPYING-BSD.
//=================================


// Test flags
//<-- XCOS TEST -->
//<-- NO CHECK ERROR OUTPUT -->

//Load xcos test diagram for geomag_model tests
[macros,path] = libraryinfo('xcos_aerolib');
result = importXcosDiagram(path + "../tests/unit_tests" +"/geomag_model.zcos");
assert_checktrue(result);

//This diagram uses 3 variables : 
//  cjd_in : cjd time
//  pos_in : position when the field is beeing measured
//  b_out  : magnetic field b vector output (to_workspace)


//======= Case 1 ====================================================

// Define parameters
cjd_in = 23294.551;
pos_in = [5000000; 5000000; 5000000];

// Define expected values
exp_time = [0.1; 1.1; 2.1];
b = [-1; -1; 1];
b_expected = [    - 0.00001261625895485,  - 0.00001221378207988,    0.00000115398181708;  
		  - 0.00001261625895485,  - 0.00001221378207988,    0.00000115398181708;  
		  - 0.00001261625895485,  - 0.00001221378207988,    0.00000115398181708  ];
scicos_simulate(scs_m);

//Validate results
assert_checkequal(exp_time, b_out.time);
assert_checkalmostequal(b_expected, b_out.values,1e-10);

//======= Case 1 ====================================================

// Define parameters
cjd_in = 23294.551;
pos_in = [5000000; 5000000; 5000100];

// Define expected values
exp_time = [0.1; 1.1; 2.1];
b = [-1; -1; 1];
b_expected = [  - 0.00001261608435739,  - 0.00001221362217269,    0.00000115360814628;  
		- 0.00001261608435739,  - 0.00001221362217269,    0.00000115360814628;  
		- 0.00001261608435739,  - 0.00001221362217269,    0.00000115360814628  ];
scicos_simulate(scs_m);

//Validate results
assert_checkequal(exp_time, b_out.time);
assert_checkalmostequal(b_expected, b_out.values,1e-10);
