// This file is released under the 3-clause BSD license. See COPYING-BSD.
//=================================
//
// Test is conducted against CelestLab function

// Test flags
//<-- XCOS TEST -->
//<-- NO CHECK ERROR OUTPUT -->

//Load xcos test diagram for eci_to_ecf tests
[macros,path] = libraryinfo('xcos_aerolib');
result = importXcosDiagram(path + "../tests/unit_tests" +"/frame_conv_jacobian.zcos");
assert_checktrue(result);

//This diagram uses 4 variables : 
//  pos_in  : position input vectors
//  vel_in  : velocity input vectors
//  jacob_out : jacobian of the transformation (to_workspace)

pos_in = [3500.e3;2500.e3;5800.e3];
vel_in = [1.e3;3.e3;7.e3];
ut1_tref = 1;
tt_tref = 67.184;
xp = 0.1; 
yp = 0.2;
dX = 0.3;
dY = 0.4;
cjd = [0.3,0.6,0.9];
use_interp = %t;

scicos_simulate(scs_m);

// Define expected values
exp_time = cjd'; 
[exp_pos, exp_vel, exp_jacob] = CL_fr_convert('ECI', 'ECF', cjd, pos_in, vel_in, ut1_tref, tt_tref, xp, yp, dX, dY, use_interp);


//Validate results
assert_checkalmostequal(exp_time, jacob_out.time, %eps);
assert_checkalmostequal(exp_jacob, jacob_out.values,%eps);

