// This file is released under the 3-clause BSD license. See COPYING-BSD.
//=================================

// Test flags
//<-- XCOS TEST -->
//<-- NO CHECK ERROR OUTPUT -->
//<-- NO CHECK REF -->
//<-- TEST WITH GRAPHIC -->

//Load xcos test diagram for plot_visibility tests
[macros,path] = libraryinfo('xcos_aerolib');
result = importXcosDiagram(path + "../tests/unit_tests/plot_visibility.zcos");
assert_checktrue(result);

// Simulate and save plot
scicos_simulate(scs_m);
h = gcf();
save(path + "../tests/unit_tests/test_data/result_visibility_plot.bin", 'h');
close(h);

//Validate results
assert_checkfilesequal(path + "../tests/unit_tests/test_data/result_visibility_plot.bin",...
    path + "../tests/unit_tests/test_data/expected_visibility_plot.bin");

result = deletefile(path + "../tests/unit_tests/test_data/result_visibility_plot.bin");
