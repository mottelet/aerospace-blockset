// This file is released under the 3-clause BSD license. See COPYING-BSD.
//=================================
//
// Test is conducted against CelestLab function

// Test flags
//<-- XCOS TEST -->
//<-- NO CHECK ERROR OUTPUT -->

//Load xcos test diagram for eci_to_ecf tests
[macros,path] = libraryinfo('xcos_aerolib');
result = importXcosDiagram(path + "../tests/unit_tests" +"/frame_conv_dcm.zcos");
assert_checktrue(result);

//This diagram uses 4 variables : 
//  cjd_tref : modified julian day since 1950
//  dcm_out : DCM of the transformation (to_workspace)
//  omega_out : omega vactor of the transformation (to_workspace)

tt_tref = 67.184;
xp = 0.1; 
yp = 0.2;
dX = 0.3;
dY = 0.4;
cjd_tref = [21010];
ut1_tref=-0.2
use_interp = %t;


scicos_simulate(scs_m);

// Define expected values
exp_time = [0.3; 0.6; 0.9]; 
[M_exp, omega_exp] = CL_fr_convertMat("ECI", "ECF", cjd_tref, ut1_tref, tt_tref, xp, yp, dX, dY, use_interp);


//Validate results
assert_checkalmostequal(exp_time, dcm_out.time, %eps);
assert_checkalmostequal(M_exp, dcm_out.values(:,:,1),%eps);
assert_checkalmostequal(M_exp, dcm_out.values(:,:,2),%eps);
assert_checkalmostequal(M_exp, dcm_out.values(:,:,3),%eps);
assert_checkalmostequal(omega_exp', omega_out.values(1,:),%eps);
assert_checkalmostequal(omega_exp', omega_out.values(2,:),%eps);
assert_checkalmostequal(omega_exp', omega_out.values(3,:),%eps);

