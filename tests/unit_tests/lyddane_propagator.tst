// This file is released under the 3-clause BSD license. See COPYING-BSD.
//=================================

// Test flags
//<-- XCOS TEST -->
//<-- NO CHECK ERROR OUTPUT -->

//Load xcos test diagram for lyddane_propagator tests
[macros,path] = libraryinfo('xcos_aerolib');
result = importXcosDiagram(path + "../tests/unit_tests" + "/lyddane_propagator.xcos");
assert_checktrue(result);

//This diagram uses 12 variables : 
//    dga_in - Semi-major axis [m] (input value)
//    ecc_in - Eccentricity (input value)
//    inc_in - Inclination [rad] (input value)
//    pom_in - Periapsis argument [rad] (input value)
//    gom_in - Right ascension of ascending node [rad] (input value)
//    anm_in - Mean anomaly [rad] (input value)
//    dga_out - Semi-major axis [m] (output value)
//    ecc_out - Eccentricity (output value)
//    inc_out - Inclination [rad] (output value)
//    pom_out - Periapsis argument [rad] (output value)
//    gom_out - Right ascension of ascending node [rad] (output value)
//    anm_out - Mean anomaly [rad] (output value)


// Define input values
dga_in = 7070.e3;
ecc_in = 0.001;
inc_in = 1.7104227;
pom_in = 0;
gom_in = 0;
anm_in = 0;

// Define Expected values
dga_exp = [7070000.; 7070000.; 7070000.; 7070000.; 7070000.; 7070000.; 7070000.; 7070000.];
ecc_exp = [0.001;0.001;0.001;0.001;0.001;0.001;0.001;0.001];
inc_exp = [1.7104227; 1.7104227; 1.7104227; 1.7104227; 1.7104227; 1.7104227; 1.7104227; 1.7104227];
pom_exp = [0.;   -0.0003795; -0.0007589; -0.0011384; -0.0015178; -0.0018973; -0.0022767; -0.0026562 ];
gom_exp = [0.; 0.0001169; 0.0002337; 0.0003506; 0.0004674; 0.0005843; 0.0007012; 0.0008180];
anm_exp = [0.; 0.6368219; 1.2736439; 1.9104658; 2.5472878; 3.1841097; 3.8209316; 4.4577536];
time_exp = [0; 0.0069444; 0.0138888; 0.0208332; 0.0277776; 0.034722; 0.0416664; 0.0486108];

// Define Expected values
dga2_exp = [7079181.9; 7072668.1; 7062387.; 7062889.9; 7073409.4; 7079114.5; 7071994.6; 7062049.3];
ecc2_exp = [0.0018410; 0.0013683; 0.0004168; 0.0017507; 0.0019360; 0.0012005; 0.0018762; 0.0030330];
inc2_exp = [1.7104227; 1.7104227; 1.7104227; 1.7104227; 1.7104227; 1.7104227; 1.7104227; 1.7104227];
pom2_exp = [0.6559310; 1.1928165; -0.6757578; -0.0947909; 0.7563744; 1.095875; 0.5845897; 1.0134033];
gom2_exp = [-0.0000004; 0.0000287; 0.0001824; 0.0004085; 0.0005526; 0.0005764; 0.0006107; 0.0007716];
anm2_exp = [-0.6559291; -0.5554545; 1.9491783; 2.0035149; 1.7885071; 2.086417; 3.2350036; 3.4421731];

// Simulate
scicos_simulate(scs_m);

//Validate results
assert_checkequal(dga_out.values, dga_exp);
assert_checkequal(ecc_out.values, ecc_exp);
assert_checkequal(inc_out.values, inc_exp);
assert_checkalmostequal(pom_out.values, pom_exp, 1e-3);
assert_checkalmostequal(gom_out.values, gom_exp, 1e-3);
assert_checkalmostequal(anm_out.values, anm_exp, 1e-4);

assert_checkalmostequal(dga2_out.values, dga2_exp, 1e-4);
assert_checkalmostequal(ecc2_out.values, ecc2_exp, 1e-4);
assert_checkalmostequal(inc2_out.values, inc2_exp, 1e-4);
assert_checkalmostequal(pom2_out.values, pom2_exp, 1e-3);
assert_checkalmostequal(gom2_out.values, gom2_exp, 1e-1);
assert_checkalmostequal(anm2_out.values, anm2_exp, 1e-4);

assert_checkalmostequal(dga_out.time, time_exp, 1e-4);

//clear result dga_in ecc_in inc_in pom_in gom_in anm_in dga_exp ecc_exp inc_exp pom_exp gom_exp anm_exp time_exp dga_out ecc_out inc_out pom_out gom_out anm_out scs_m;
