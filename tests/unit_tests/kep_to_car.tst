// This file is released under the 3-clause BSD license. See COPYING-BSD.
//=================================


// Test flags
//<-- XCOS TEST -->
//<-- NO CHECK ERROR OUTPUT -->

//Load xcos test diagram for kep_to_car tests
[macros,path] = libraryinfo('xcos_aerolib');
result = importXcosDiagram(path + "../tests/unit_tests" +"/kep_to_car.xcos");
assert_checktrue(result);

//This diagram uses 3 variables : 
//  kep_in : keplerian element input vectors
//  pos_out : cartesian position output vectors (to_workspace)
//  vel_out : cartesian velocity output vectors (to_workspace)

kep_in=[ 24464560,  8427837.5;
      0.7311,    1.822631;
      0.122138,  1.5707963;
      3.10686,   0.;
      1.00681,   1.5707963;
      0.048363  -2.220D-16];
scicos_simulate(scs_m);

// Define expected values
exp_time = [0.1; 1.1; 2.1];
position = [-1076225.3,  0.1857690;
            -6765896.4,  6933000.4; 
            -332308.78, -3.466D-09];
velocity = [ 9356.8574, -0.0003413;
            -3312.3476,  1.140D-11;
            -1188.0157, 12739.];

//Validate results
assert_checkequal(exp_time, pos_out.time);
assert_checkequal(exp_time, vel_out.time);
assert_checkalmostequal(position, pos_out.values(:,:,1),1e-4);
assert_checkalmostequal(position, pos_out.values(:,:,2),1e-4);
assert_checkalmostequal(position, pos_out.values(:,:,3),1e-4);
assert_checkalmostequal(velocity, vel_out.values(:,:,1),1e-3);
assert_checkalmostequal(velocity, vel_out.values(:,:,2),1e-3);
assert_checkalmostequal(velocity, vel_out.values(:,:,3),1e-3);

