// This file is released under the 3-clause BSD license. See COPYING-BSD.
//=================================

// Test flags
//<-- XCOS TEST -->
//<-- NO CHECK ERROR OUTPUT -->

//Load xcos test diagram for cjd_now tests
[macros,path] = libraryinfo('xcos_aerolib');
result = importXcosDiagram(path + "../tests/unit_tests" + "/cjd_now.zcos");
assert_checktrue(result);

// Define Expected values
current_cjd = CL_dat_now("cjd");
cjd_diff = 1/(1440*60);
time_exp = (0.1:1:4.1)';


// Clear variables used for outputs
clear('cjd');

// Simulate
scicos_simulate(scs_m);

//Validate results
assert_checkalmostequal(cjd.values(1), current_cjd, 1E-8);
assert_checkalmostequal(cjd.values(2)-cjd.values(1), cjd_diff, 1E-6);
assert_checkalmostequal(cjd.values(3)-cjd.values(2), cjd_diff, 1E-6);
assert_checkalmostequal(cjd.values(4)-cjd.values(3), cjd_diff, 1E-6);
assert_checkalmostequal(cjd.values(5)-cjd.values(4), cjd_diff, 1E-6);
assert_checkalmostequal(cjd.time, time_exp, 1E-8);
