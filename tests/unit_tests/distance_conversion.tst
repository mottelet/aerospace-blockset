// This file is released under the 3-clause BSD license. See COPYING-BSD.
//=================================


// Test flags
//<-- XCOS TEST -->
//<-- NO CHECK ERROR OUTPUT -->

//Load xcos test diagram for DISTANCE_CONVERSION tests
[macros,path] = libraryinfo('xcos_aerolib');
result = importXcosDiagram(path + "../tests/unit_tests" +"/km2m.zcos");
assert_checktrue(result);

//This diagram uses variable : 
//  distance_from : distance to convert (from_workspace)

distance_from = [0, 97.8; -10, -0.198];
scicos_simulate(scs_m);
exp_value = hypermat([2 2 3]);


// Define expected values
exp_time = [0.1; 1.1; 2.1];
exp_value(:,:,1) = [0, 97800; -10000, -198];
exp_value(:,:,2) = [0, 97800; -10000, -198];
exp_value(:,:,3) = [0, 97800; -10000, -198];

//Validate results
assert_checkequal(exp_time, distance_to.time);
assert_checkequal(exp_value, distance_to.values);


//Load xcos test diagram for DISTANCE_CONVERSION tests
result = importXcosDiagram(path + "../tests/unit_tests" +"/km2au.zcos");
assert_checktrue(result);

//This diagram uses variable : 
//  distance_from : distance to convert (from_workspace)

distance_from = [123, -98756.5; 0, 9999999999];
scicos_simulate(scs_m);

// Define expected values
exp_time = [0.1; 1.1; 2.1];
exp_value(:,:,1) = [0.0000008, -0.0006601; 0, 66.845871];
exp_value(:,:,2) = [0.0000008, -0.0006601; 0, 66.845871];
exp_value(:,:,3) = [0.0000008, -0.0006601; 0, 66.845871];

//Validate results
assert_checkequal(exp_time, distance_to.time);
assert_checkalmostequal(exp_value(:,:,1), distance_to.values(:,:,1), 1e-1);
assert_checkalmostequal(exp_value(:,:,2), distance_to.values(:,:,2), 1e-1);
assert_checkalmostequal(exp_value(:,:,3), distance_to.values(:,:,3), 1e-1);
