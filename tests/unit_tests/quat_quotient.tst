// This file is released under the 3-clause BSD license. See COPYING-BSD.
//=================================

// Test flags
//<-- XCOS TEST -->
//<-- NO CHECK ERROR OUTPUT -->

//Load xcos test diagram for quat_quotient tests
[macros,path] = libraryinfo('xcos_aerolib');
result = importXcosDiagram(path + "../tests/unit_tests" + "/quat_quotient.zcos");
assert_checktrue(result);

//This diagram uses 9 variables : 
//  r_in - real quaternion part
//  i_in -
//  j_in -  imaginary quaternion parts
//  k_in -
//  r2_in - real second quaternion part
//  i2_in -
//  j2_in -  imaginary second quaternion parts
//  k2_in -
//  quat_out - resulting quaternion

r_in = -31.6;
i_in = 20.3;
j_in = 17.7;
k_in = 4.4;
r2_in = 2;
i2_in = 3;
j2_in = 1;
k2_in = 4;

scicos_simulate(scs_m);

// Define expected values
time_expected = [0.1; 1.1; 2.1];
quat_expected = [   1.1,    2.3,    4.5,    5.6;  
                    1.1,    2.3,    4.5,    5.6;  
                    1.1,    2.3,    4.5,    5.6 ]
                  
//Validate results
assert_checkequal(quat_out.time, time_expected);
assert_checkalmostequal(quat_out.values, quat_expected, 1e-6);
