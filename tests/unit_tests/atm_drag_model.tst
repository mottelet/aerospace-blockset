// This file is released under the 3-clause BSD license. See COPYING-BSD.
//=================================

// Test flags
//<-- XCOS TEST -->
//<-- NO CHECK ERROR OUTPUT -->

//Load xcos test diagram for atm_drag_model tests
[macros,path] = libraryinfo('xcos_aerolib');
result = importXcosDiagram(path + "../tests/unit_tests" + "/atm_drag_model.zcos");
assert_checktrue(result);

//This diagram uses variables : 
// V_in		- velocity vector
// rho_in	- atmsphere density
// cd_in	- drag coefficient
// A_in		- surface area
// F_out	- force vector

// Define input values
V_in	= [	1;
			2;
			3];
rho_in	= [	0.5];
cd_in	= [	0.25];
A_in	= [	1.2];


scicos_simulate(scs_m);

// Define expected values
exp_time = [0.1; 1.1; 2.1];
exp_F = [ -0.2806243040080456	-0.5612486080160912	-0.84187291202413683;
	  -0.2806243040080456	-0.5612486080160912	-0.84187291202413683;
	  -0.2806243040080456	-0.5612486080160912	-0.84187291202413683 ];

//Validate results
assert_checkequal(F_out.time, exp_time);
assert_checkalmostequal(F_out.values, exp_F, 1e-7);

