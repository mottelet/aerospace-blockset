// Determining tests to execute

base_path = get_absolute_file_path('run_unit_tests.sce');
s = filesep();
f = findfiles(base_path + s + 'unit_tests', '*.tst');
number_of_tests = size(f);

//Giving basic info

mprintf("\nThere are %d tests to execute:\n\n", number_of_tests(1));

for i = 1:number_of_tests(1)
	mprintf("EXECUTING %i\t%s", i, f(i));
    try
	   exec(base_path + s + 'unit_tests' + s + f(i), -1);
       mprintf("\n");
    catch
       mprintf("\t\t - FAILED!!\n");
    end
end
