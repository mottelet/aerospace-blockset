//
//  Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
//  Copyright (C) 2013 - Pawel Zagorski
//
//  This file must be used under the terms of the CeCILL.
//  This source file is licensed as described in the file COPYING, which
//  you should have received as part of this distribution.  The terms
//  are also available at
//  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function block=AB_quatInvert(block,flag)

previousprot = funcprot(0)

block_name = "AB_quatInvert";
//xcos_block_debug(block, flag, block_name);
if scicos_debug() == 2 then xcos_block_debug(block, flag, block_name); end

select flag
  case 1 then
  // Output computation
    r = block.inptr(1)(1);
    i1 = block.inptr(1)(2);
    i2 = block.inptr(1)(3);
    i3 = block.inptr(1)(4);
    quat = CL_rot_defQuat(r,i1,i2,i3);
    q = inv(quat);
    block.outptr(1)(1) = q.r;
    block.outptr(1)(2) = q.i(1);
    block.outptr(1)(3) = q.i(2);
    block.outptr(1)(4) = q.i(3);
  case 4 then
  // Initialization
    AB_check_param_number(block_name, block, 0, 0, 0);
  case 5 then
  // Ending
    ;
  case 6 then
  // Initialization, fixed-point computation
    ;
  else
    error(msprintf(gettext("%s : Incorect flag %d"), block_name, flag));
end

if scicos_debug() == 2 then xcos_block_perf(); end
//xcos_block_perf();
funcprot(previousprot);

endfunction
