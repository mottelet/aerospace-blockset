//
// This file is part of the Xcos Aerospace Blockset
//
// Copyright (C) 2014 - Pawel Zagorski
// see license.txt for more licensing informations

function [x,y,typ]=ELL_TO_CAR(job,arg1,arg2)
  x=[];y=[];typ=[];
  jacobian = %f;   // This is different for other block using AB_fr_convert()
  select job
   case 'plot' then
    standard_draw(arg1);
   case 'getinputs' then
    [x,y,typ]=standard_inputs(arg1);
   case 'getoutputs' then
    [x,y,typ]=standard_outputs(arg1);
   case 'getorigin' then
    [x,y]=standard_origin(arg1);
   case 'set' then
    x=arg1; //in ’set’ x is the data structure of the block
    graphics=arg1.graphics;
    exprs=graphics.exprs;
    model=arg1.model;

    while %t do
      labels = ["Equatorial radius of reference ellipsoid [m]"; "Oblateness of reference ellipsoid"];
      types = list('col', 1, 'col', 1);
      [ok, er, obla, exprs]=getvalue(["Set ELL_TO_CAR block parameters"],labels, types, exprs);

      if ~ok then break,end

      //check validity of parameters
      if obla>=1 then
	block_parameter_error(msprintf(gettext("Wrong value for ''%s'' parameter"), gettext("Oblateness of reference ellipsoid")), ..
          gettext("Value must be lower than 1"));
        ok=%f;
      end 
      if er<=0 then
        block_parameter_error(msprintf(gettext("Wrong value for ''%s'' parameter"), gettext("Equatorial radius of reference ellipsoid")), ..
          gettext("Value must be greater than 0"));
        ok=%f;
      end 
      //...
      //[model,graphics,ok]=set_io(model,graphics,in,out,...
      //clkin,clkout,in_implicit,out_implicit)
      //...
      if ok then
        //model.ipar	= [];
        model.rpar	= [er, obla];
        graphics.exprs=exprs;
        x.graphics=graphics;
        x.model=model
        break
      else
        message("Failed to update block io");
      end
    end
   case 'define' then
    er = 6378136.2999999998137355;
    obla = 0.0033528106647474804902 ;

    model	= scicos_model();
    model.sim	= list('AB_co_ell2car',5);
    // two inputs with a single "double" element
    model.in	= [3];
    model.in2	= [1];
    model.intyp	= [1];
    // one output with a single "double" element
    model.out	= [3;3];
    model.out2	= [1;3];

    // block parameters
    model.ipar	= [];
    model.rpar	= [er, obla];
    model.opar = list();

    model.blocktype='c';
    model.dep_ut=[%t %f];

    exprs=[
		string(er);
		string(obla);
	  ];

    gr_i=['txt=[''ELL_TO_CAR''];';
          'xstringb(orig(1),orig(2),txt,sz(1),sz(2),''fill'')'];
    //gr_i=['txt=[''Frame conversion''];';
    //      'xstringb(orig(1),orig(2),txt,sz(1),sz(2),''fill'')'];

    x=standard_define([2 2],model,exprs,gr_i);
    graphics=x.graphics;
    graphics.style = "fillColor=white";
    //graphics.id=frame_IN+"->"+frame_OUT;
    //graphics.in_label=["xyz";"V";"cjd"];
    //graphics.out_label=["xyz";"V"];
    x.graphics=graphics;
  end
endfunction

