//
//  Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
//  Copyright (C) 2012 - Pawel Zagorski
//
//  This file must be used under the terms of the CeCILL.
//  This source file is licensed as described in the file COPYING, which
//  you should have received as part of this distribution.  The terms
//  are also available at
//  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function AB__plot_earthMap(win_id,color_id,tick_steps,data_bounds,thickness,res,coord)

if exists('win_id','local') then
  f = scf(win_id)
else
  f = gcf();
end

// error checking

if (size(data_bounds,1) <> 2 | size(data_bounds,2) <> 2)
  CL__error('Invalid size for data_bounds');
end
if (data_bounds(2,1)<=data_bounds(1,1) | data_bounds(2,1)>data_bounds(1,1)+360)
  CL__error('Invalid data bounds in longitude');
end 
if (data_bounds(2,2)<=data_bounds(1,2) | data_bounds(1,2) < -90 | data_bounds(2,2) > 90)
  CL__error('Invalid data bounds in latitude');
end 

if (tick_steps <> [])
  if (length(tick_steps) <> 2)
    CL__error('Invalid size for tick_steps');
  end 
  if (tick_steps(1) <= 0 | tick_steps(2) <= 0)
    CL__error('Invalid values for tick_steps');
  end 
end

// plot
immediate_drawing_save = f.immediate_drawing; // store field
f.immediate_drawing = "off"; 

// loading of the map (blocks separated by %nan)
// pts: size = 3xN
pts = CL_getEarthMap(res, coord); 

// interval in longitude containing the view
bmin = (data_bounds(1,1)+data_bounds(2,1))/2 - 180; 
bmax = (data_bounds(1,1)+data_bounds(2,1))/2 + 180; 

 // plot curve (degrees)
[x, y] = CL__plot_genxy(pts(1,:)*180/%pi, pts(2,:)*180/%pi, bmin, bmax);
//k = 1; 
//for i=1:size(xN, 2)
//    if ~or(isnan([xN(1,i) yN(1,i)]))
//	x(1,k) = xN(1,i);
//	y(1,k) = yN(1,i);
//	k=k+1;
//    end
//end 
box = [data_bounds(1,1), data_bounds(1,2), data_bounds(2,1),data_bounds(2,2)];
plot2d(x, y);
h = CL_g_select(gce(), "Polyline"); 
h.foreground = color_id; 
h.thickness = thickness; 


// adjustments (grid, databounds)
a = gca();
a.data_bounds = data_bounds;
a.tight_limits = "on";

if (tick_steps <> [])
  bticks = [ tick_steps(1)*floor(data_bounds(1,:)/tick_steps(1)); 
             tick_steps(2)*ceil((data_bounds(2,:))/tick_steps(2)) ];

  x_ticks = bticks(1,1) : tick_steps(1) : bticks(2,1);
  y_ticks = bticks(1,2) : tick_steps(2) : bticks(2,2);

  // strsplit+msprintf : used to avoid "string"
  x_ticks_labels = strsplit( stripblanks( msprintf("%.8g ",x_ticks')) , " ")' ; 
  y_ticks_labels = strsplit( stripblanks( msprintf("%.8g ",y_ticks')) , " ")' ;

  a.x_ticks = tlist(["ticks", "locations", "labels"], x_ticks, x_ticks_labels ); 
  a.y_ticks = tlist(["ticks", "locations", "labels"], y_ticks, y_ticks_labels ); 
end

CL_g_stdaxes(a);

f.immediate_drawing = immediate_drawing_save; // restore field

endfunction
