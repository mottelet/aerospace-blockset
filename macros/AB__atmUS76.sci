// Declarations:
// ----------------------------------------------
// Internal function
// ----------------------------------------------
function [temp,pres,dens] = AB__atmUS76(alt, rntab)

  N = size(alt,2);
  temp = zeros(1,N);
  pres = zeros(1,N);
  dens = zeros(1,N);

  // ----------------------------------------------
  // Initialisations
  // ----------------------------------------------

  rhpot = zeros(1,N);
  tmol = zeros(1,N);
  xmol = zeros(1,N);
  coef = zeros(1,N);

  // alts geopotentielles de debut et de fin des differents gradients de temp
  htab = [0.,11e3,20e3,32e3,47e3,51e3,71e3,84.852e3,0.,0.,0.,0.,0.];

  // alts geometriques
  ztab = [ 0.,0.,0.,0.,0.,0.,0.,86e3,91e3,110e3,120e3,500e3,1000e3 ];

  // coeficients correcteurs pour le calcul de la temp  entre 80 et 86 km d'alt
  xmm0tb = [ 1.,0.999996,0.999989,0.999971,0.999941,0.999909,0.999870,0.999829,0.999786,0.999741,0.999694,0.999641,0.999579 ];

  // derivees de la temp par rapport a rhpot ou rzgeo
  dttab = [ -6.5e-3,0.,1e-3,2.8e-3,0.,-2.8e-3,-2.0e-3,0.,0.,12e-3,0.,0.,0. ];

  // press pour htab
  ptab = [ 1.01325e5,2.2632e4,5.4748e3,8.6801e2,1.1090e2,6.6938e1,3.9564,3.7338e-1 ];

  // temps correspondant a htab ou ztab
  ttab = [0.,0.,0.,0.,0.,0.,0.,186.8673,186.8673,240.,360.,999.24,1000. ];

  tmtab = [ 288.15,216.65,216.65,228.65,270.65,270.65,214.65,186.946 ];

  // masses molaires des elements N2,O,O2,Ar,He et H
  rmoltb = [ 0.,28.0134,15.9994,31.9988,39.948, 4.0026,1.0080 ];

  // rayon de la Terre utilise pour le calcul des alts geopotentielles
  r0 = 6356.766e3;  

  // acceleration due a la gravite au niveau de la mer
  g0 = 9.80665;     

  // masse moleculaire de l'atmosphere au sol (kg/kmol)
  xmol0 = 28.9644; 

  // constante de Boltzmann (n.m/k)     
  boltz = 1.380622e-23; 

  // constante des gaz parfaits (n.m/(kmol.k))
  rstar = 8.31432e3; 

  rappor = g0*xmol0/rstar;

  // rlambd = dttab(1+1+9)/(ttab(1+12)-ttab(1+10)) ???
  rlambd = 1.875e-05; 
  // attention: valeur pour des alts en metres

  // epsilon de test du gradient de temp pour le calcul de la pres
  epspre = 1e-06; 

  tc = 263.1905;
  gda_ = -76.3232;
  pta = -19.9429e3;

  // composition de l'atmosphere en fonction des differents
  // elements qui la composent: N2,O,O2,Ar,He et H et en fonction
  // de l'alt a partir de 86 km
   


  // ----------------------------------------------
  // calcul de temp (t), pres (p) et xmol (m) 
  // pour alt < ou = 86 km
  // ----------------------------------------------

  I_alt = find(alt <= 86.e3);  

  if (I_alt <> [])

    // calcul de l'alt geopotentielle h [eq. 18]  
    rhpot(I_alt) = r0 .* alt(I_alt) ./ (r0+alt(I_alt)); 
      
    // recherche de l'intervalle d'alt encadrant rhpot tel que:
    // htab(ii) <= rhpot < htab(ii+1)

    ii = ones(1:N);

    for i = 2:7
       j = find(htab(i) <= rhpot(I_alt)); 
       ii(I_alt(j)) = i;
    end

    // calcul de la temp moleculaire tm [eq. 23]
    tmol(I_alt) = tmtab(ii(I_alt)) + dttab(ii(I_alt)) .* (rhpot(I_alt)-htab(ii(I_alt)));  

    // calcul de la temp cinetique t et de la masse molaire m:
    // pour des alts z < 80 km
    I = find(alt < 80e3);

    if (I <> [])
      temp(I) = tmol(I);
      xmol(I) = xmol0;
    end 

    // pour des alts z > ou = 80 km
    I = find(alt >= 80e3 & alt <= 86.e3); 

    if (I <> [])
      // recherche de l'intervalle d'altitude encadrant alt 
      // tel que: 80km + 500m*(j) < alt <= 80km + 500m*(j+1)

      jj  = max(ceil((alt - 80.e3)/500)-1, 0); 
      zj = 80000 + jj * 500; // *** A VERIFIER ***

      // interpolation (en fonction de l'alt) du rapport m/m0:
      coef(I) = xmm0tb(1+jj(I)) + (xmm0tb(1+jj(I)+1)-xmm0tb(1+jj(I)))/500 .* (alt(I)-zj(I));
      // calcul de la temp cinetique t (avec m/m0 interpole) [eq. 22]
      temp(I) = tmol(I) .* coef(I);  
      // calcul de la masse molaire m (interpolee)
      xmol(I) = xmol0 .* coef(I) 
    end 

    //  calcul de la pres p:
    //  test sur le gradient de temp
    I = find((abs(dttab(ii)) < epspre) & (alt <= 86.e3));

    if (I <> [])
      pres(I) = ptab(ii(I)) .* exp(-rappor .* (rhpot(I) - htab(ii(I))) ./ tmtab(ii(I)));  //[eq. 33b]
    end

    I = find((abs(dttab(ii)) >= epspre) & (alt <= 86.e3));

    if (I <> [])
      pres(I) = ptab(ii(I)) .* (tmtab(ii(I))./tmol(I)) .^ (rappor./dttab(ii(I)));  // [eq. 33a]
    end

  end 


  // ----------------------------------------------
  // calcul de temp (t), pres (p) et xmol (m) 
  // pour alt > 86 km
  // calcul de la temp cinetique t
  // ----------------------------------------------

  I_alt = find(alt > 86.e3);   

  if (I_alt <> [])

    I = find((alt < ztab(1+8)) & (alt > 86.e3)); 
    temp(I) = ttab(1+7);   // [eq. 25]

    I = find((alt < ztab(1+9)) & (~(alt < ztab(1+8))) & (alt > 86.e3)); 
    if (I <> [])
      rcar = sqrt( 1-( ( alt(I)-ztab(1+8) )./pta ).^2 ); // [eq. 27]
      temp(I) = tc + gda_ .* rcar;
    end

    I = find((alt < ztab(1+10)) & (~(alt < ztab(1+8))) & (~(alt < ztab(1+9))) & (alt > 86.e3));
    if (I <> [])
      temp(I) = ttab(1+9) + dttab(1+9) .* (alt(I)-ztab(1+9));
    end

    I = find((~(alt < ztab(1+8))) & (~(alt < ztab(1+9))) & (~(alt < ztab(1+10))) & (alt > 86.e3));
    if (I <> [])
      xi = (alt(I)-ztab(1+10)) .* (r0+ztab(1+10)) ./ (r0+alt(I))  // [eq. 31];
      temp(I) = ttab(1+12) - (ttab(1+12)-ttab(1+10)) .* exp(-rlambd*xi);
    end

    // calcul des number densities "Ni" pour chaque element N2,O,O2,Ar,He et H:

    // recherche de l'intervalle d'alt encadrant alt tel que
    // rntab(K,1) < alt <= rntab(K+1,1)

    // construction des blocs ou le pas sur l'altitude (rntab) est
    // constant => D
    // D contient par ligne: indice_deb, alt_deb, alt_fin, pas_alt

    [val, ind] = unique(rntab(2:$,1) - rntab(1:$-1,1)); 
    ind = [ind; size(rntab,1)];
    l = length(ind);
    D = [ ind(1:l-1), rntab(ind(1:l-1),1), rntab(ind(2:l),1), ...
          rntab(ind(1:l-1)+1,1)-rntab(ind(1:l-1),1) ]; 
    
    K = zeros(1,N);

    for j = 1:l-1
      I = find(K == 0 & alt <= D(j,3)); 
      if (I <> [])
         K(I) = ceil((alt(I) - D(j,2)) ./ D(j,4)) + D(j,1) - 1; 
      end
    end
    
    pen = zeros(7,N);
    rnn = zeros(7,N);
    pen(2:7,I_alt) = ((rntab(K(I_alt)+1,2:7)-rntab(K(I_alt),2:7)) ./ ((rntab(K(I_alt)+1,1)-rntab(K(I_alt),1)).*.ones(1,6)))';

    // interpolation suivant l'alt pour chaque "ni"
    rnn(2:7,I_alt) = (rntab(K(I_alt),2:7) + pen(2:7,I_alt)' .* ((alt(I_alt)'-rntab(K(I_alt),1)).*.ones(1,6)))';  
   
    // contribution a la somme totale de chaque "ni"
    xmol(I_alt) = CL_dot(rnn(2:7,I_alt),(rmoltb(2:7)').*.ones(1,size(I_alt,2)));   
                  
    rn = sum(rnn(2:7,I_alt),1); 

    // calcul de la masse molaire m [eq. 20]
    xmol(I_alt) = xmol(I_alt) ./ rn;    

    // calcul de la pres p [eq. 33c]
    pres(I_alt) = rn .* boltz .* temp(I_alt);            

  end 

  // calcul de la dens rho  [eq. 42]
  dens = (pres .* xmol) ./ (rstar .* temp);    

endfunction
