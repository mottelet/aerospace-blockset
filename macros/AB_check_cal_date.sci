//
//  Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
//  Copyright (C) 2012 - Pawel Zagorski
//
//  This file must be used under the terms of the CeCILL.
//  This source file is licensed as described in the file COPYING, which
//  you should have received as part of this distribution.  The terms
//  are also available at
//  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function err=AB_check_cal_date(cal, time, label)
  err = 1;
  year		= cal(1);
  month 	= cal(2);
  day		= cal(3);
  hour		= cal(4);
  minute	= cal(5);
  second	= cal(6);
  if and(cal == zeros(6,1)) then
    err = 0;
    if time <> 0 then
      warning(msprintf("\t%s : Encountered empty input vector at time = %f!\n", label, time));
    end
  elseif floor(year) <> year then
    err = -1;     
    messagebox("Input year must be an integer number.", label, "error"); 
  elseif (floor(month) <> month) | (month < 1) | (month > 12) then
    err = -1;     
    messagebox("Input month must be an integer number from 1 to 12.", label, "error"); 
  elseif (floor(day) <> day) | (day < 1) | (day > 31)  then
    err = -1;     
    messagebox("Input day must be an integer number from 1 to 31.", label, "error"); 
  elseif (floor(hour) <> hour) | (hour < 0) | (hour > 23)  then
    err = -1;     
    messagebox("Input hour must be an integer number from 0 to 23.", label, "error"); 
  elseif (floor(minute) <> minute) | (minute < 0) | (minute > 59)  then
    err = -1;     
    messagebox("Input minute must be an integer number from 0 to 59.", label, "error"); 
  elseif (second < 0) | (second >= 60)  then
    err = -1;     
    messagebox("Input second must be an real number not lower than 0 and lower than 60.", label, "error"); 
  end
endfunction
