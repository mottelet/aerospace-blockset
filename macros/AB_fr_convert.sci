//
//  Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
//  Copyright (C) 2013 - Pawel Zagorski
//
//  This file must be used under the terms of the CeCILL.
//  This source file is licensed as described in the file COPYING, which
//  you should have received as part of this distribution.  The terms
//  are also available at
//  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function block=AB_fr_convert(block,flag)

previousprot = funcprot(0)

block_name = "AB_fr_convert";
//xcos_block_debug(block, flag, block_name);
if scicos_debug() == 2 then xcos_block_debug(block, flag, block_name); end

select flag
  case 1 then
    // Output computation
    pos1 = block.inptr(1);
    vel1 = block.inptr(2);
    cjd     = block.inptr(3);   // Modified Julian date from 1950.0 (TREF time scale) (1xN or 1x1)
    jacobian = block.opar(1);   // (boolean) Should jacobian be calculated?
    frame1 = block.opar(2);     // (string) Initial frame (1x1)
    frame2 = block.opar(3);     // (string) Final frame (1x1)
    pcor = block.opar(4);       // (optional) Position of the pole in ITRS [rad] (1xN or 1x1)
    cipcor = block.opar(5);     // (optional) Corrections to CIP coordinates [rad] (1xN or 1x1)
    ut1_tref = block.rpar(1);   // (optional) UT1-TREF [seconds]. Default is %CL_UT1_TREF (1xN or 1x1)
    tt_tref = block.rpar(2);    // (optional) TT-TREF [seconds]. Default is %CL_TT_TREF. (1xN or 1x1)
    opt = 'pos_vel';            // opt = flag specifying to the internal function CL__fr_convert what needs to be computed
    use_interp = %t;            // (optional, boolean) %t to use interpolation when computing CIP coordinates.
    
    // Should jacobian be calculated
    if(jacobian) then
        opt = 'pos_vel_jac';
    end
        
    // Arguments structure to be used in all sub-functions :
    if use_interp then 
        model = 'interp';
    else
        model = 'classic';
    end,
    
    args = struct('model', model, 'precession_model', '2006', 'nutation_model', '2000AR06', 'ut1_tt', ut1_tref - tt_tref, 'xp', pcor(1), 'yp', pcor(2), 'dx06', cipcor(1), 'dy06', cipcor(2));
  
    // Convert input date from MJD1950.0 (TREF time scale)
    // to a two part JD (TT time scale)
    // ttjda + ttjdb = cjd + 2433282.5;
    ttjda = floor(cjd) + 2433282;
    ttjdb = cjd - floor(cjd) + 0.5 + tt_tref/86400;
    ttjd = [ttjda;ttjdb];
        
	[M,omega,pos2,vel2,jacob] = CL__fr_convert(frame1, frame2, ttjd, pos1, vel1, opt, args);

    // Set block outputs
    if(jacobian) then
	   block.outptr(1) = jacob;
    else
    	block.outptr(1) = pos2;
    	block.outptr(2) = vel2; 
    end

  case 4 then
    // Initialization
    AB_check_param_number(block_name, block, 0, 1, 4);
  case 5 then
    // Ending
    ;
  case 6 then
    // Initialization, fixed-point computation
    ;
  else
    error(msprintf(gettext("%s : Incorect flag %d"), block_name, flag));
end

if scicos_debug() == 2 then xcos_block_perf(); end
//xcos_block_perf();
funcprot(previousprot);

endfunction
