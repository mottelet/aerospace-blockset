//
// This file is part of the Xcos Aerospace Blockset
//
// Copyright (C) 2014 - Pawel Zagorski
// see license.txt for more licensing informations

function [x,y,typ]=TIMEZONE_CONVERSION(job,arg1,arg2)
  x=[];y=[];typ=[];
  select job
   case 'plot' then
    standard_draw(arg1);
   case 'getinputs' then
    [x,y,typ]=standard_inputs(arg1);
   case 'getoutputs' then
    [x,y,typ]=standard_outputs(arg1);
   case 'getorigin' then
    [x,y]=standard_origin(arg1);
   case 'set' then
    x=arg1; //in ’set’ x is the data structure of the block
    graphics=arg1.graphics;
    exprs=graphics.exprs;
    model=arg1.model;
    while %t do
      labels = ['Time difference [full hours]'];
      types = list('vec', 1);
      [ok, timezone, exprs]=scicos_getvalue(..
        "Set TIMEZONE_CONVERSION time difference", labels, types, exprs);

      if ~ok then break,end
      mess=[];
      if timezone < -24  then
        mess=[mess;'Timezone difference can not be smaller than -24h';' ']
        ok=%f
      end
      if timezone > 24 then
        mess=[mess;'Timezone difference can not be greater than -24h';' ']
        ok=%f
      end
      
      if ~ok then
        message(['Some specified values are inconsistent:';
	         ' ';mess])
      end
      //...
      //[model,graphics,ok]=set_io(model,graphics,in,out,...
      //clkin,clkout,in_implicit,out_implicit)
      //...
      if ok then
        model.ipar = [timezone];
        model.rpar = [];
        graphics.exprs=exprs;
        x.graphics=graphics;
        x.model=model;
        break
      else
        message("Failed to update block io");
      end
    end
   case 'define' then
    timezone = 1;

    model	= scicos_model();
    model.sim	= list('AB_timezone_conversion',5);
    // no inputs
    model.in	= [6];
    model.in2	= [1];
    model.intyp	= [1];
    // one output with a single "double" element
    model.out	= [6];
    model.out2	= [1];
    model.outtyp= [1];
    // block parameters
    model.ipar	= [timezone];
    model.rpar	= [];
    model.opar	= list();    
        
    // discrete states
    model.odstate = list(zeros(1));

    model.blocktype='c';
    model.dep_ut=[%t %f];

    exprs=string([timezone]);
    gr_i=['txt=[''TIMEZONE_CONVERSION''];';
          'xstringb(orig(1),orig(2),txt,sz(1),sz(2),''fill'')'];

    x=standard_define([2 2],model,exprs,gr_i);
    graphics=x.graphics;
    graphics.style = "fillColor=white";
    x.graphics=graphics;
  end
endfunction

