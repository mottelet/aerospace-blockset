//
//  Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
//  Copyright (C) 2014 - Pawel Zagorski
//
//  This file must be used under the terms of the CeCILL.
//  This source file is licensed as described in the file COPYING, which
//  you should have received as part of this distribution.  The terms
//  are also available at
//  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function block=AB_timezone_conversion(block,flag)

previousprot = funcprot(0)

block_name = "AB_timezone_conversion";
block_label = "TIMEZONE_CONVERSION";
//xcos_block_debug(block, flag, block_name);
if scicos_debug() == 2 then xcos_block_debug(block, flag, block_name); end

select flag
  case 1 then
  // Output computation
    timediff = block.oz(1);
    cal = block.inptr(1);

    cjd = CL_dat_cal2cjd(cal);
    cjd = cjd + timediff;
    cal = CL_dat_cjd2cal(cjd);

	block.outptr(1) = cal
  case 4 then
  // Initialization
    AB_check_param_number(block_name, block, 1, 0, 0);
    block.oz(1) = block.ipar(1) / 24;
  case 5 then
  // Ending
    ;
  case 6 then
  // Initialization, fixed-point computation
    ;
  else
    error(msprintf(gettext("%s : Incorect flag %d"), block_name, flag));
end

if scicos_debug() == 2 then xcos_block_perf(); end
//xcos_block_perf();
funcprot(previousprot);

endfunction
