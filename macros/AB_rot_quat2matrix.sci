//
//  Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
//  Copyright (C) 2014 - Pawel Zagorski
//
//  This file must be used under the terms of the CeCILL.
//  This source file is licensed as described in the file COPYING, which
//  you should have received as part of this distribution.  The terms
//  are also available at
//  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function block=AB_rot_quat2matrix(block,flag)

previousprot = funcprot(0);

block_name = "AB_rot_quat2matrix";
//xcos_block_debug(block, flag, block_name);
if scicos_debug() == 2 then xcos_block_debug(block, flag, block_name); end

select flag
  case 1 then
  // Output computation
    vel_conv = block.opar(1);
    if vel_conv == 1 then
        q = CL_rot_defQuat(block.inptr(1));
        qdot = CL_rot_defQuat(block.inptr(2));
        [dcm, omega] = CL_rot_quat2matrix(q, qdot);
        block.outptr = list(dcm,omega);
    elseif vel_conv == 0 then
        q = CL_rot_defQuat(block.inptr(1));
        dcm = CL_rot_quat2matrix(q);
        block.outptr = list(dcm);
    else
        error('Wrong parameters specyfing parameters conversion in AB_rot_quat2matrix');
        set_blockerror(-1);
    end
  case 4 then
  // Initialization
    AB_check_param_number(block_name, block, 0, 1, 0);
  case 5 then
  // Ending
    ;
  case 6 then
  // Initialization, fixed-point computation
    ;
  else
    error(msprintf(gettext("%s : Incorect flag %d"), block_name, flag));
end

if scicos_debug() == 2 then xcos_block_perf(); end
//xcos_block_perf();
funcprot(previousprot);

endfunction
