//
// This file is part of the Xcos Aerospace Blockset
//
// Copyright (C) 2012 - Pawel Zagorski
// see license.txt for more licensing informations

function [x,y,typ]=DISTANCE_CONVERSION(job,arg1,arg2)
  x=[];y=[];typ=[]
  select job
   case 'plot' then
    from = arg1.graphics.exprs(1);
    to = arg1.graphics.exprs(2);
    label = "$\mbox{" + from + "}\rightarrow\mbox{" + to + "}$";
    standard_draw(arg1);
   case 'getinputs' then
    [x,y,typ]=standard_inputs(arg1)
   case 'getoutputs' then
    [x,y,typ]=standard_outputs(arg1)
   case 'getorigin' then
    [x,y]=standard_origin(arg1)
   case 'set' then
    x=arg1; //in ’set’ x is the data structure of the block
    graphics=arg1.graphics;
    exprs=graphics.exprs;
    model=arg1.model;
    while %t do
      labels = ['Convert from:';
                'Convert to:'];
      types = list( 'str', 1, 'str', 1);
      [ok, unit_from, unit_to, ..
          exprs]=getvalue(["Set distance units to convert"; "- m  - meters"; "- km  - kilometers"; "- au  - astronomical units"],labels, types, exprs);

      if ~ok then break,end

      //check validity of parameters
      if and(unit_from <> ["m" "km" "au"]) then
        block_parameter_error(msprintf(gettext("Wrong value for ''%s'' parameter"), gettext("Convert from")), ..
          gettext("Value must be a distance unit (m, km, au)."));
        ok=%f
      end 
      if and(unit_to <> ["m" "km" "au"]) then
        block_parameter_error(msprintf(gettext("Wrong value for ''%s'' parameter"), gettext("Convert to")), ..
          gettext("Value must be a distance unit (m, km, au)."));
        ok=%f
      end
      if (unit_from == unit_to) then
        block_parameter_error(msprintf(gettext("Cannot convert between the same distance units")), gettext("Use different unit for conversion"));
        ok=%f
      end

      if ok then
        model.opar = list(unit_from, unit_to);
        graphics.exprs=exprs;
        x.graphics=graphics;
        x.model=model
        break
      else
        message("Failed to update block io");
      end
    end
   case 'define' then
    unit_from = "km";
    unit_to   = "m";

    model           = scicos_model()
    model.sim       = list('AB_unit_conversion',5)
    model.in        = [-1]
    model.in2       = [-2]
    model.intyp     = [1]
    model.out       = [-1]
    model.out2      = [-2]
    model.outtyp    = [1]
    model.opar      = list(unit_from, unit_to);
    model.blocktype = 'c'
    model.dep_ut    = [%t %f]
    gr_i=['dx=sz(1)/5;dy=sz(2)/10;'
      'w=sz(1)-2*dx;h=sz(2)-2*dy;'
      'txt=label;'
      'xstringb(orig(1)+dx,orig(2)+dy,txt,w,h,''fill'');'
      'warning(''redrawing block'')']
    exprs=[
           unit_from;
           unit_to
          ]
          
        
    // discrete states
    model.odstate = list(zeros(1),zeros(1));
    
    //gr_i=['dx=sz(1)/5;dy=sz(2)/10;';
    //  'w=sz(1)-2*dx;h=sz(2)-2*dy;';
    //  'txt=label;'
    //  'xstringb(orig(1)+dx,orig(2)+dy,txt,w,h,''fill'');']
    x=standard_define([2 2],model,exprs,gr_i)
    graphics=x.graphics;
    graphics.style = "fillColor=white";
    x.graphics=graphics;
  end
endfunction

