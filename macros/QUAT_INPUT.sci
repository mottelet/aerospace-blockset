//
// This file is part of the Xcos Aerospace Blockset
//
// Copyright (C) 2012 - Pawel Zagorski
// see license.txt for more licensing informations

function [x,y,typ]=QUAT_INPUT(job,arg1,arg2)
  x=[];y=[];typ=[]
  select job
   case 'plot' then
    standard_draw(arg1)
   case 'getinputs' then
    [x,y,typ]=standard_inputs(arg1)
   case 'getoutputs' then
    [x,y,typ]=standard_outputs(arg1)
   case 'getorigin' then
    [x,y]=standard_origin(arg1)
   case 'set' then
    x=arg1; //in ’set’ x is the data structure of the block
    graphics=arg1.graphics;
    exprs=graphics.exprs;
    model=arg1.model;
    while %t do
      labels = ['Real part';
                'Imaginary i';
                'Imaginary j';
                'Imaginary k'];
      types = list( 'col', 1, 'col', 1, 'col', 1, 'col', 1);
      [ok,real_r, img_i, img_j, img_k, ..
          exprs]=scicos_getvalue("Set QUAT_INPUT",labels, types, exprs);
      if ~ok then break,end
      if ok then
        model.rpar = [real_r img_i img_j img_k];
        graphics.exprs=exprs;
        x.graphics=graphics;
        x.model=model
        break
      end
    end
   case 'define' then
    real_r = 0;
    img_i = 0;
    img_j = 0;
    img_k = 0;

    model=scicos_model();
    model.sim=list('AB_quat_input',5);
    // no inputs
    model.in=[];
    model.intyp=[];
    // one output with a six "double" elements
    model.out=[4];
    model.out2=[1];
    model.outtyp=[1];
    
    //model.opar = list(quat);
    //model.nopar = 1;
    model.blocktype='d';
    model.dep_ut=[%f %t];

    exprs=[
       string(real_r);
       string(img_i);
       string(img_j);
       string(img_k);
      ]

    gr_i=['txt=[''QUAT_INPUT''];';
          'xstringb(orig(1),orig(2),txt,sz(1),sz(2),''fill'')'];

    x=standard_define([2 2],model,exprs,gr_i);
    graphics=x.graphics;
    graphics.style = "fillColor=white";
    x.graphics=graphics;
  end
endfunction

