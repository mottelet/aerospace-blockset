//
//  Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
//  Copyright (C) 2014 - Pawel Zagorski
//
//  This file must be used under the terms of the CeCILL.
//  This source file is licensed as described in the file COPYING, which
//  you should have received as part of this distribution.  The terms
//  are also available at
//  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function block=AB_rot_matrix2quat(block,flag)

previousprot = funcprot(0);

block_name = "AB_rot_matrix2quat";
//xcos_block_debug(block, flag, block_name);
if scicos_debug() == 2 then xcos_block_debug(block, flag, block_name); end

select flag
  case 1 then
  // Output computation
    vel_conv = block.opar(1);
    if vel_conv == 1 then
        dcm = block.inptr(1);
        omega = block.inptr(2);
        [q, qdot] = CL_rot_matrix2quat(dcm, omega);
        block.outptr = list([q.r(1); q.i(1); q.i(2); q.i(3)],[qdot.r(1); qdot.i(1); qdot.i(2); qdot.i(3)]);
    elseif vel_conv == 0 then
        dcm = block.inptr(1);
        q = CL_rot_matrix2quat(dcm);
        block.outptr = list([q.r(1); q.i(1); q.i(2); q.i(3)]);
    else
        error('Wrong parameters specyfing parameters conversion in AB_rot_matrix2quat');
        set_blockerror(-1);
    end
  case 4 then
  // Initialization
    AB_check_param_number(block_name, block, 0, 1, 0);
  case 5 then
  // Ending
    ;
  case 6 then
  // Initialization, fixed-point computation
    ;
  else
    error(msprintf(gettext("%s : Incorect flag %d"), block_name, flag));
end

if scicos_debug() == 2 then xcos_block_perf(); end
//xcos_block_perf();
funcprot(previousprot);

endfunction
