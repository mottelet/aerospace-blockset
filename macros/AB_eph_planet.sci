//
//  Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
//  Copyright (C) 2013 - Pawel Zagorski
//
//  This file must be used under the terms of the CeCILL.
//  This source file is licensed as described in the file COPYING, which
//  you should have received as part of this distribution.  The terms
//  are also available at
//  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function block=AB_eph_planet(block,flag)

previousprot = funcprot(0)

block_name = "AB_eph_planet";
//xcos_block_debug(block, flag, block_name);
if scicos_debug() == 2 then xcos_block_debug(block, flag, block_name); end

select flag
  case 1 then
  // Output computation
    planet = block.opar(1);
    mod = block.opar(2);
    cjd = block.inptr(1);
    [eod_pos, eod_vel] = CL_eph_planet(planet, cjd, model=mod);
    block.outptr(1) = eod_pos;
    block.outptr(2) = eod_vel;
  case 4 then
  // Initialization
    if block.nin<>1 | block.nout<>2 then
      error('Incorrect number of block inputs or output for calling CL_mod_moonSun function');
      set_blockerror(-1);
    end
    AB_check_param_number(block_name, block, 0, 1, 0);
  case 5 then
  // Ending
    ;
  case 6 then
  // Initialization, fixed-point computation
    ;
  else
    error(msprintf(gettext("%s : Incorect flag %d"), block_name, flag));
end

if scicos_debug() == 2 then xcos_block_perf(); end
//xcos_block_perf();
funcprot(previousprot);

endfunction

