//
//  Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
//  Copyright (C) 2012 - Pawel Zagorski
//
//  This file must be used under the terms of the CeCILL.
//  This source file is licensed as described in the file COPYING, which
//  you should have received as part of this distribution.  The terms
//  are also available at
//  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt



function []=AB_check_param_number(block_name, block, nipar, nopar, nrpar)

    if block.nipar <> nipar then 
      mprintf("\n");
      warning(msprintf(gettext("\t%s : Incorrect value of model.nipar, should be %d instead of %d \n"),..
            block_name, block.nipar, nipar)); 
    end
    if block.nopar <> nopar then 
      mprintf("\n");
      warning(msprintf(gettext("\t%s : Incorrect value of model.nopar, should be %d instead of %d \n"),..
            block_name, block.nopar, nopar)); 
    end
    if block.nrpar <> nrpar then 
      mprintf("\n");
      warning(msprintf(gettext("\t%s : Incorrect value of model.nrpar, should be %d instead of %d \n"),..
            block_name, block.nrpar, nrpar)); 
    end

endfunction
