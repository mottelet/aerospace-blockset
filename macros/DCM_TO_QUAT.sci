//
// This file is part of the Xcos Aerospace Blockset
//
// Copyright (C) 2014 - Pawel Zagorski
// see license.txt for more licensing informations

function [x,y,typ]=DCM_TO_QUAT(job,arg1,arg2)
  x=[];y=[];typ=[]
  select job
   case 'plot' then
    standard_draw(arg1)
   case 'getinputs' then
    [x,y,typ]=standard_inputs(arg1)
   case 'getoutputs' then
    [x,y,typ]=standard_outputs(arg1)
   case 'getorigin' then
    [x,y]=standard_origin(arg1)
   case 'set' then
    x=arg1; //in ’set’ x is the data structure of the block
    graphics=arg1.graphics;
    exprs=graphics.exprs;
    model=arg1.model;
    while %t do
      labels = ['Convert angular rate (0/1)'];
      types = list('col', 1);
      [ok, vel_conv, exprs]=scicos_getvalue(..
       "Set DCM_TO_QUAT block parameters",labels, types, exprs);

      if ~ok then break,end
      mess=[]
      if vel_conv<>0 & vel_conv<>1 then
        mess=[mess;'Convert angular rate must have value of either 1 or 0';' ']
        ok=%f
      end
      if ~ok then
        message(['Some specified values are inconsistent:';
	         ' ';mess])
      end

      if ok then
         if vel_conv == 0  then
            model.in=[3];
            model.in2=[3];
            model.intyp=[1];
            model.out=[4];
            model.out2=[1];
            model.outtyp=[1];
            model.opar = list(vel_conv);
         elseif vel_conv == 1
            model.in=[3;3];
            model.in2=[3;1];
            model.intyp=[1;1];
            model.out=[4;4];
            model.out2=[1;1];
            model.outtyp=[1;1];
            model.opar = list(vel_conv);
         end
      end

      if ok then
        //model.evtin= ones(1-herited,1);
        graphics.exprs=exprs;
        x.graphics=graphics;
        x.model=model
        break
      end
    end

   case 'define' then
    model=scicos_model()
    model.sim=list('AB_rot_matrix2quat',5)

    model.in=[3;3];
    model.in=[3;1];
    model.intyp=[1;1];

    model.out=[4;4];
    model.out=[1;1];
    model.outtyp=[1;1];

    model.blocktype='c';
    model.dep_ut=[%t %f];

    // Default value of vel_conv
    model.opar = list(1);
    exprs=string(["1"]);
    
    gr_i=['txt=[''DCM_TO_QUAT''];';
          'xstringb(orig(1),orig(2),txt,sz(1),sz(2),''fill'')'];
    x=standard_define([2 2],model,exprs,gr_i);
    graphics=x.graphics;
    graphics.style = "fillColor=white";
    x.graphics=graphics;
  end
endfunction

