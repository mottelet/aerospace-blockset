//
//  Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
//  Copyright (C) 2013 - Pawel Zagorski
//
//  This file must be used under the terms of the CeCILL.
//  This source file is licensed as described in the file COPYING, which
//  you should have received as part of this distribution.  The terms
//  are also available at
//  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function block=AB_gm_stationPointing(block,flag)

previousprot = funcprot(0)

block_name = "AB_gm_stationPointing";
block_label = "STATION_POINTING";
//xcos_block_debug(block, flag, block_name);
if scicos_debug() == 2 then xcos_block_debug(block, flag, block_name); end

select flag
  case 1 then
    // Output computation
    lat		= block.inptr(1)(1);
    long	= block.inptr(1)(2);
    alt		= block.inptr(1)(3);
    mask	= block.inptr(1)(4);
    pos_x	= block.inptr(2)(1);
    pos_y	= block.inptr(2)(2);
    pos_z	= block.inptr(2)(3);

    // Check inputs validity
    if (lat < - %pi/2) | (lat > %pi/2)  then
      messagebox("Lattitude must be between -PI/2 and PI/2 radians (positive north).", block_label, "error");
      set_blockerror(-1);
    end
    if (long < -%pi) | (long > %pi)  then
      messagebox("Longitude must be between -PI and PI radians (positive east).", block_label, "error");
      set_blockerror(-1);
    end
    if (mask >= %pi/2) | (mask <= - %pi/2)  then
      messagebox("Mask must be greater than -PI/2 and less than PI/2 radians (positive up).", block_label, "error");
      set_blockerror(-1);
    end

    station 	= [long;lat;alt];
    pos_ECF	= [pos_x;pos_y;pos_z];
    [azim, elev, dist] = CL_gm_stationPointing(station, pos_ECF);
    block.outptr(1)(1) = azim;
    block.outptr(1)(2) = elev;
    block.outptr(2) = dist;    
  case 3 then
    ;
  case 4 then
  // Initialization
    AB_check_param_number(block_name, block, 0, 0, 0);
  case 5 then
  // Ending
    ;
  case 6 then
  // Initialization, fixed-point computation
    ;
  else
    error(msprintf(gettext("%s : Incorect flag %d"), block_name, flag));
end

if scicos_debug() == 2 then xcos_block_perf(); end
//xcos_block_perf();
funcprot(previousprot);

endfunction
