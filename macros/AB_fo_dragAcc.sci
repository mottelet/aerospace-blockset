//
//  Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
//  Copyright (C) 2013 - Pawel Zagorski
//
//  This file must be used under the terms of the CeCILL.
//  This source file is licensed as described in the file COPYING, which
//  you should have received as part of this distribution.  The terms
//  are also available at
//  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function block=AB_fo_dragAcc(block,flag)

previousprot = funcprot(0)

block_name = "AB_fo_dragAcc";
block_label = "ATM_DRAG_MODEL";
//xcos_block_debug(block, flag, block_name);
if scicos_debug() == 2 then xcos_block_debug(block, flag, block_name); end

select flag
  case 1 then
  // Output computation
    vel =		block.inptr(1);
    rho =		block.inptr(2);
    drag_coeff =	block.inptr(3);
    A =			block.inptr(4);
    if (rho < 0)
      set_blockerror(-1);      
      error(msprintf(gettext("\t%s\t: Air density must be greater than zero."), block_label)); 
    elseif (drag_coeff <= 0)
      set_blockerror(-1);      
      error(msprintf(gettext("\t%s\t: Drag coefficient must be greater than zero."), block_label)); 
    elseif (A <= 0)
      set_blockerror(-1);      
      error(msprintf(gettext("\t%s\t: Cross-section area must be greater than zero."), block_label)); 
    else
      //we are NOT calculating acceleration here, because we do not difide drag_coeff*A by the mass
      K = -0.5 .* rho .* drag_coeff * A .* CL_norm(vel); 
      block.outptr(1) = CL_dMult(vel, K);

      //block.outptr(1) = CL_fo_dragAcc(vel, rho, drag_coeff * A);
    end
  case 4 then
   //Initialization
    if block.nin<>4 then
      set_blockerror(-1);      
      error(msprintf(gettext("\t%s\t: Incorrect number of block inputs or output for calling CL_fo_dragAcc() function"), block_label, "error"));
    end
  case 5 then
  // Ending
    ;
  case 6 then
  // Initialization, fixed-point computation
    ;
  else
    //error(msprintf(gettext("%s : Incorect flag %d"), block_name, flag));
end

funcprot(previousprot);
xcos_block_perf();
endfunction

