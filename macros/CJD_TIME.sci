//
// This file is part of the Xcos Aerospace Blockset
//
// Copyright (C) 2012 - Pawel Zagorski
// see license.txt for more licensing informations

function [x,y,typ]=CJD_TIME(job,arg1,arg2)
  x=[];y=[];typ=[];
  select job
   case 'plot' then
    standard_draw(arg1);
   case 'getinputs' then
    [x,y,typ]=standard_inputs(arg1);
   case 'getoutputs' then
    [x,y,typ]=standard_outputs(arg1);
   case 'getorigin' then
    [x,y]=standard_origin(arg1);
   case 'set' then
    x=arg1; //in ’set’ x is the data structure of the block
    graphics=arg1.graphics;
    exprs=graphics.exprs;
    model=arg1.model;
    while %t do
      labels = ['Time multiplier (0 for stopped clock)';
                'Year';
                'Month';
                'Day';
                'Hour'; 
                'Minute';
                'Second'
               ];
      types = list('vec', 1, 'vec', 1, 'vec', 1, 'vec', 1,..
                   'vec', 1, 'vec', 1, 'vec', 1);
      [ok, multiplier, year, month, day, hour, minute,second, exprs]=scicos_getvalue(..
        "Set CJD_TIME initial time and multiplier", labels, types, exprs);

      if ~ok then break,end
      mess=[];
      if multiplier < 0  then
        mess=[mess;'Multiplier can not be negative';' ']
        ok=%f
      end
      if year < -4712 then
        mess=[mess;'Year should be integer number greater or equal to -4712';' ']
        ok=%f
      end
      if month < 1 | month > 12  then
        mess=[mess;'Month should be integer number between 1 and 12';' ']
        ok=%f
      end
      if day < 1 | day > 31  then
        mess=[mess;'Day should be integer number between 1 and 31';' ']
        ok=%f
      end
      if hour < 0 | hour > 23  then
        mess=[mess;'Hour should be integer number between 0 and 23';' ']
        ok=%f
      end
      if minute < 0 | minute > 60  then
        mess=[mess;'Hour should be integer number between 0 and 59';' ']
        ok=%f
      end
      if second < 0 | second >= 60  then
        mess=[mess;'Second should be real number not lower than 0 and lower than 60';' ']
        ok=%f
      end
      if ~ok then
        message(['Some specified values are inconsistent:';
	         ' ';mess])
      end
      //...
      //[model,graphics,ok]=set_io(model,graphics,in,out,...
      //clkin,clkout,in_implicit,out_implicit)
      //...
      if ok then
        model.ipar = [year,month,day,hour,minute];
        model.rpar = [multiplier,second];
        graphics.exprs=exprs;
        x.graphics=graphics;
        x.model=model;
        break
      else
        message("Failed to update block io");
      end
    end
   case 'define' then
    multiplier = 1440;
    year = 2010;
    month=1;
    day=1;
    hour=0;
    minute = 0;
    second = 0;

    model	= scicos_model();
    model.sim	= list('AB_cjd_time',5);
    // no inputs
    model.in	= [];
    model.in2	= [];
    model.intyp	= [];
    // one output with a single "double" element
    model.out	= [1];
    model.out2	= [1];
    model.outtyp= [1];
    // block parameters
    model.ipar	= [year,month,day,hour,minute];
    model.rpar	= [multiplier,second];
    model.opar	= list();    
        
    // discrete states
    model.odstate = list(zeros(1));

    model.blocktype='c';
    model.dep_ut=[%f %t];

    exprs=string([multiplier;year;month;day;hour;minute;second]);
    gr_i=['txt=[''CJD_TIME''];';
          'xstringb(orig(1),orig(2),txt,sz(1),sz(2),''fill'')'];

    x=standard_define([2 2],model,exprs,gr_i);
    graphics=x.graphics;
    graphics.style = "fillColor=white";
    x.graphics=graphics;
  end
endfunction

