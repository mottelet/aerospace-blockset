//
// This file is part of the Xcos Aerospace Blockset
//
// Copyright (C) 2012 - Pawel Zagorski
// see license.txt for more licensing informations

function [x,y,typ]=VECTOR_NORMALIZATION(job,arg1,arg2)
  x=[];y=[];typ=[]
  select job
   case 'plot' then
    standard_draw(arg1);
   case 'getinputs' then
    [x,y,typ]=standard_inputs(arg1);
   case 'getoutputs' then
    [x,y,typ]=standard_outputs(arg1);
   case 'getorigin' then
    [x,y]=standard_origin(arg1);
   case 'set' then
    x=arg1; //in ’set’ x is the data structure of the block
   case 'define' then
    model           = scicos_model();
    model.sim       = list('AB_unitVector',5);
    model.in        = [-1];
    model.in2       = [-2];
    model.intyp     = [1];
    model.out       = [-1];
    model.out2      = [-2];
    model.outtyp    = [1];
    model.blocktype = 'c';
    model.dep_ut    = [%t %f];

    exprs=[];
    gr_i=['txt=[''VECTOR_NORMALIZATION''];';
          'xstringb(orig(1),orig(2),txt,sz(1),sz(2),''fill'')'];
    x=standard_define([2 2],model,exprs,gr_i);
    graphics=x.graphics;
    graphics.style = "fillColor=white";
    x.graphics=graphics;
  end
endfunction

