//
// This file is part of the Xcos Aerospace Blockset
//
// Copyright (C) 2012 - Pawel Zagorski
// see license.txt for more licensing informations

function [x,y,typ]=KEPLERIAN_INPUT(job,arg1,arg2)
  x=[];y=[];typ=[]
  select job
   case 'plot' then
    standard_draw(arg1)
   case 'getinputs' then
    [x,y,typ]=standard_inputs(arg1)
   case 'getoutputs' then
    [x,y,typ]=standard_outputs(arg1)
   case 'getorigin' then
    [x,y]=standard_origin(arg1)
   case 'set' then
    x=arg1; //in ’set’ x is the data structure of the block
    graphics=arg1.graphics;
    exprs=graphics.exprs;
    model=arg1.model;
    while %t do
      labels = ['Semi-major axis [m]';
                'Eccentricity';
                'Inclination [deg]';
                'Periapsis argument [deg]'; 
                'Right ascension of ascending node [deg]';
                'Mean anomaly [deg]'];
      types = list( 'col', 1, 'col', 1, 'col', 1, 'col', 1, ..
          'col', 1, 'col', 1);
      [ok,sma, ecc, incl, periapsis, raan, manom, ..
          exprs]=scicos_getvalue("Set KEPLERIAN_INPUT",labels, types, exprs);
      if ~ok then break,end
      mess=[]
      if ecc<0  then
        mess=[mess;'Eccentricity must be greater or equal 0';' ']
        ok=%f
      end
      if sma<=0  then
        mess=[mess;'Semi-major axis must be greater than 0';' ']
        ok=%f
      end
      if incl>180 | incl<0  then
        mess=[mess;'Inclination must be between 0 and 180 degrees';' ']
        ok=%f
      end
      if ~ok then
        message(['Some specified values are inconsistent:';
	         ' ';mess])
      end
      if ok then
        model.rpar = [sma ecc incl periapsis raan manom];
        graphics.exprs=exprs;
        x.graphics=graphics;
        x.model=model
        break
      end
    end
   case 'define' then
    sma = 7070.e3;
    ecc = 0.001;
    incl = 97.97;
    periapsis = 0;
    raan = 0;
    manom = 0;

    model=scicos_model();
    model.sim=list('AB_keplerian_input',5);
    // no inputs
    model.in=[];
    model.intyp=[];
    // one output with a six "double" elements
    model.out=[6];
    model.out2=[1];
    model.outtyp=[1];
    
    model.rpar = [sma ecc incl periapsis raan manom];
    model.blocktype='d';
    model.dep_ut=[%f %t];

    exprs=[
       string(sma);
       string(ecc);
       string(incl);
       string(periapsis);
       string(raan);
       string(manom)
      ]

    gr_i=['txt=[''KEPLERIAN_INPUT''];';
          'xstringb(orig(1),orig(2),txt,sz(1),sz(2),''fill'')'];

    x=standard_define([2 2],model,exprs,gr_i);
    graphics=x.graphics;
    graphics.style = "fillColor=white";
    x.graphics=graphics;
  end
endfunction

