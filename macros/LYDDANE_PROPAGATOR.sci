//
// This file is part of the Xcos Aerospace Blockset
//
// Copyright (C) 2012 - Pawel Zagorski
// see license.txt for more licensing informations

function [x,y,typ]=LYDDANE_PROPAGATOR(job,arg1,arg2)
  x=[];y=[];typ=[]
  select job
   case 'plot' then
    standard_draw(arg1);
   case 'getinputs' then
    [x,y,typ]=standard_inputs(arg1);
   case 'getoutputs' then
    [x,y,typ]=standard_outputs(arg1);
   case 'getorigin' then
    [x,y]=standard_origin(arg1);
   case 'set' then
    x=arg1; //in ’set’ x is the data structure of the block
    graphics=arg1.graphics;
    exprs=graphics.exprs;
    model=arg1.model;
    while %t do
      labels = [	'Orbital elements type (mean/osc)']
      types = list( 'str', 1);
      [ok,elements_type, exprs]=scicos_getvalue(["Set LYDDANE_PROPAGATOR parameter";"Input orbital elements should be mean or osculationg (type in mean or osc)"],..
		labels, types, exprs);
      if ok then
        model.opar = list(elements_type);
        graphics.exprs=exprs;
        x.graphics=graphics;
        x.model=model
        break
      end
    end
   case 'define' then
    elements_type = ["mean"];

    model=scicos_model();
    model.sim=list('AB_ex_lyddane',5);
    // three inputs: keplerian elements, cjd epoch, current cjd time
    model.in=[6;1;1];
    model.in2=[1;1;1];
    model.intyp=[1;1;1];
    // one output with a six "double" elements
    model.out=[6;6];
    model.out2=[1;1];
    model.outtyp=[1;1];

    // Event input for clock signal:
    model.evtin = 1;
    
    model.blocktype='d';
    model.dep_ut=[%f %t];

    model.opar = list(elements_type);

    exprs=[
      elements_type
      ]
    gr_i=['txt=[''LYDDANE_PROPAGATOR''];';
          'xstringb(orig(1),orig(2),txt,sz(1),sz(2),''fill'')'];

    x=standard_define([2 4],model,exprs,gr_i);
    graphics=x.graphics;
    graphics.style = "fillColor=white";
    x.graphics=graphics;
  end
endfunction

