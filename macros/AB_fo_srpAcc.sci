//
//  Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
//  Copyright (C) 2013 - Pawel Zagorski
//
//  This file must be used under the terms of the CeCILL.
//  This source file is licensed as described in the file COPYING, which
//  you should have received as part of this distribution.  The terms
//  are also available at
//  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function block=AB_fo_srpAcc(block,flag)

previousprot = funcprot(0)

block_name = "AB_fo_srpAcc";
block_label = "SOL_DRAG_MODEL";
//xcos_block_debug(block, flag, block_name);
if scicos_debug() == 2 then xcos_block_debug(block, flag, block_name); end

select flag
  case 1 then
  // Output computation
    pos_sat =		block.inptr(1);		// object position in the inertial frame
    pos_sun =		block.inptr(2);		// sun position in the inertial frame
    cp =		block.inptr(3);		// reflectivity coefficient, between 1 and 2
    A =			block.inptr(4);		// cross-section area
    if (cp < 1 | cp > 2)
      set_blockerror(-1);      
      error(msprintf(gettext("\t%s\t: Reflectivity coefficient must be between 1 and 2."), block_label)); 
    elseif (A < 0)
      set_blockerror(-1);      
      error(msprintf(gettext("\t%s\t: Cross-section area must be greater or than zero."), block_label)); 
    else
      //we are NOT calculating acceleration here, because we do not difide cp*A by the mass
      block.outptr(1) = CL_fo_srpAcc(pos_sat, pos_sun, cp * A, %f); 
    end
  case 4 then
   //Initialization
    if block.nin<>4 then
      set_blockerror(-1);      
      error(msprintf(gettext("\t%s\t: Incorrect number of block inputs or output for calling CL_fo_srpAcc() function"), block_label, "error"));
    end
  case 5 then
  // Ending
    ;
  case 6 then
  // Initialization, fixed-point computation
    ;
  else
    //error(msprintf(gettext("%s : Incorect flag %d"), block_name, flag));
end

if scicos_debug() == 2 then xcos_block_perf(); end
//xcos_block_perf();
funcprot(previousprot);

endfunction

