//
//  Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
//  Copyright (C) 2013 - Pawel Zagorski
//
//  This file must be used under the terms of the CeCILL.
//  This source file is licensed as described in the file COPYING, which
//  you should have received as part of this distribution.  The terms
//  are also available at
//  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function block=AB_rotationJacobian(block,flag)

previousprot = funcprot(0)

block_name = "AB_rotationJacobian";
//xcos_block_debug(block, flag, block_name);
if scicos_debug() == 2 then xcos_block_debug(block, flag, block_name); end

select flag
  case 1 then
  // Output computation
    q1 = block.inptr(1)(1);
    q2 = block.inptr(1)(2);
    q3 = block.inptr(1)(3);
    q4 = block.inptr(1)(4);
    dx = block.inptr(2)(1);
    dy = block.inptr(2)(2);
    dz = block.inptr(2)(3);
    
    n1 = norm([q1 q2 q3 q4]);
    n2 = norm([dx dy dz]);
    
    if (n1 > 1 - %eps) & (n2 > 1 - %eps) & (n1 < 1 + %eps) & (n2 < 1 + %eps) then
         //this equation only works well for normalized quaternions
         out = [2*dy*q4 - 2*dz*q3, 2*dy*q3 + 2*dz*q4, -4*dx*q3 + 2*dy*q2 - 2*dz*q1, -4*dx*q4 + 2*dy*q1 + 2*dz*q2;
         -2*dx*q4 + 2*dz*q2, 2*dx*q3 - 4*dy*q2 + 2*dz*q1, 2*dx*q2 + 2*dz*q4, -2*dx*q1 - 4*dy*q4 + 2*dz*q3;
         2*dx*q3 - 2*dy*q2, 2*dx*q4 - 2*dy*q1 - 4*dz*q2, 2*dx*q1 + 2*dy*q4 - 4*dz*q3, 2*dx*q2 + 2*dy*q3]
    else
        error(msprintf(gettext("\t%s : Incorrect input for ROTATION_JACOBIAN block, quaternion and vector must be normalized. See ROTATION_JACOBIAN block help for details.\n"),..
      block_name));
    end

    block.outptr(1) = out;
  case 4 then
  // Initialization
    AB_check_param_number(block_name, block, 0, 0, 0);
  case 5 then
  // Ending
    ;
  case 6 then
  // Initialization, fixed-point computation
    ;
  else
    error(msprintf(gettext("%s : Incorect flag %d"), block_name, flag));
end

funcprot(previousprot);
xcos_block_perf();
endfunction
