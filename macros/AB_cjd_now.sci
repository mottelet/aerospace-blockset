//
//  Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
//  Copyright (C) 2014 - Pawel Zagorski
//
//  This file must be used under the terms of the CeCILL.
//  This source file is licensed as described in the file COPYING, which
//  you should have received as part of this distribution.  The terms
//  are also available at
//  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function block=AB_cjd_now(block,flag)

previousprot = funcprot(0);

block_name = "AB_cjd_now";
//xcos_block_debug(block, flag, block_name);
if scicos_debug() == 2 then xcos_block_debug(block, flag, block_name); end

select flag
  case 1 then
    // Output computation 
    cjd = CL_dat_now("cjd");
    block.outptr(1) = cjd;
  case 2 then
    ; 
  case 4 then
    // Initialization
    AB_check_param_number(block_name, block, 0, 0, 0);
    ;
  case 5 then
    // Ending
    ;
  case 6 then
    // Initialization, fixed-point computation
    ;
  else
    error(msprintf(gettext("%s : Incorect flag %d"), block_name, flag));
end

if scicos_debug() == 2 then xcos_block_perf(); end
//xcos_block_perf();
funcprot(previousprot);

endfunction
