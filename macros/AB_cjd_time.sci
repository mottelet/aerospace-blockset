//
//  Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
//  Copyright (C) 2012 - Pawel Zagorski
//
//  This file must be used under the terms of the CeCILL.
//  This source file is licensed as described in the file COPYING, which
//  you should have received as part of this distribution.  The terms
//  are also available at
//  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function block=AB_cjd_time(block,flag)

previousprot = funcprot(0);

block_name = "AB_cjd_time";
//xcos_block_debug(block, flag, block_name);
if scicos_debug() == 2 then xcos_block_debug(block, flag, block_name); end

select flag
  case 1 then
    // Output computation
    sim_time	= getscicosvars("t0");
    multiplier 	= block.rpar(1);
    
    cjd = block.oz(1);
    if ~(multiplier == 0) then 
        cjd = cjd + (sim_time / multiplier);
    end
    block.outptr(1) = cjd;
  case 2 then
    ; 
  case 4 then
    // Initialization
    AB_check_param_number(block_name, block, 5, 0, 4);
    
    // Epoch computation
    //multiplier 	= block.rpar(1);
    year 	= block.ipar(1);
    month	= block.ipar(2);
    day		= block.ipar(3);
    hour	= block.ipar(4);
    minute 	= block.ipar(5);
    second 	= block.rpar(2);
    
    block.oz(1) = CL_dat_cal2cjd(year,month,day,hour,minute,second);
  case 5 then
    // Ending
    ;
  case 6 then
    // Initialization, fixed-point computation
    ;
  else
    error(msprintf(gettext("%s : Incorect flag %d"), block_name, flag));
end

if scicos_debug() == 2 then xcos_block_perf(); end
//xcos_block_perf();
funcprot(previousprot);

endfunction
