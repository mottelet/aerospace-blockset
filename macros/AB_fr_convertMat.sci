//
//  Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
//  Copyright (C) 2014 - Pawel Zagorski
//
//  This file must be used under the terms of the CeCILL.
//  This source file is licensed as described in the file COPYING, which
//  you should have received as part of this distribution.  The terms
//  are also available at
//  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function block=AB_fr_convertMat(block,flag)

previousprot = funcprot(0)

block_name = "AB_fr_convertMat";
//xcos_block_debug(block, flag, block_name);
if scicos_debug() == 2 then xcos_block_debug(block, flag, block_name); end

select flag
case 1 then
    // Output computation
    cjd     = block.inptr(1);   // Modified Julian date from 1950.0 (TREF time scale) (1xN or 1x1)
    frame1 = block.opar(1);     // (string) Initial frame (1x1)
    frame2 = block.opar(2);     // (string) Final frame (1x1)
    pcor = block.opar(3);       // (optional) Position of the pole in ITRS [rad] (1xN or 1x1)
    cipcor = block.opar(4);     // (optional) Corrections to CIP coordinates [rad] (1xN or 1x1)
    ut1_tref = block.rpar(1);   // (optional) UT1-TREF [seconds]. Default is %CL_UT1_TREF (1xN or 1x1)
    tt_tref = block.rpar(2);    // (optional) TT-TREF [seconds]. Default is %CL_TT_TREF. (1xN or 1x1)
    use_interp = %t;            // (optional, boolean) %t to use interpolation when computing CIP coordinates.
  
    
    [DCM, omega] = CL_fr_convertMat(frame1, frame2, cjd, ut1_tref, tt_tref, pcor(1), pcor(2), cipcor(1), cipcor(2), use_interp);

    block.outptr = list(DCM,omega);

  case 4 then
    // Initialization
    AB_check_param_number(block_name, block, 0, 1, 4);
  case 5 then
    // Ending
    ;
  case 6 then
    // Initialization, fixed-point computation
    ;
  else
    error(msprintf(gettext("%s : Incorect flag %d"), block_name, flag));
end

if scicos_debug() == 2 then xcos_block_perf(); end
//xcos_block_perf();
funcprot(previousprot);

endfunction
