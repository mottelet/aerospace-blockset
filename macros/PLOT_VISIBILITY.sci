//
// This file is part of the Xcos Aerospace Blockset
//
// Copyright (C) 2012 - Pawel Zagorski
// see license.txt for more licensing informations

function [x,y,typ]=PLOT_VISIBILITY(job,arg1,arg2)
  x=[];y=[];typ=[]
  select job
   case 'plot' then
    standard_draw(arg1)
   case 'getinputs' then
    [x,y,typ]=standard_inputs(arg1)
   case 'getoutputs' then
    [x,y,typ]=standard_outputs(arg1)
   case 'getorigin' then
    [x,y]=standard_origin(arg1)
   case 'set' then
    x=arg1; //in ’set’ x is the data structure of the block
    graphics=arg1.graphics;
    exprs=graphics.exprs;
    model=arg1.model;
    while %t do
      labels = ['Accept inherited events (0/1)';
                'Names';
                'Colors'];
      types = list('col', 1, 'str', 1, 'str', 1);
      [ok ,herited, names_str, colors_str, exprs] = scicos_getvalue(..
       "Set PLOT_VISIBILITY block parameters",labels, types, exprs);

      try
	names = evstr(names_str);
      catch
        message('Names field must contain a row vector of strings.');
        ok=%f;
      end
      try
	colors = evstr(colors_str);
      catch
        message('Colors field must contain a row vector of strings.');
        ok=%f;
      end

      if ~ok then break,end
      mess = [];
      if herited<>0 & herited<>1 then
        mess=[mess;'Accept herited events must have value of either 1 or 0';' '];
        ok=%f;
      end
      if or(size(names) <> size(colors)) then
        mess=[mess;'Number of names and number of colors does not match';' '];
        ok=%f;
      end
      for k=1:size(colors, "*")
        try
	  color(colors(k));
        catch
          mess=[mess;'Each of the colors must be a string accepted by color() function.';' '];
          ok=%f
          break;
        end
      end
      for k=1:size(names, "*")
        if type(names(k)) <> 10 then
          mess=[mess;'Each of the names must be a string';' '];
          ok=%f;
          break;
        end
      end
      //close(f);
      if ~ok then
        message(['Some specified values are inconsistent:';
	         ' ';mess]);
      end

      if ok then
        [model,graphics,ok] = set_io(model,graphics,list([1 1],..
          [1]),list(),ones(1-herited,1),[])
      end

      if ok then
        model.ipar 	= [herited];
        model.opar 	= list(names, colors);
        model.evtin	= ones(1-herited,1);
	s 		= size(names);
    	model.in	= [ones(s(2),1);-1];
   	model.in2	= [ones(s(2),1);-2];
    	model.intyp	= [ones(s(2),1);1];
        graphics.exprs	= exprs;
        x.graphics	= graphics;
        x.model		= model;
        break
      end
    end

   case 'define' then
    herited = 1;
    names = ["Corou", "KSC"];
    colors = ["blue", "green"];
    s 	= size(names);

    model=scicos_model();
    model.sim=list('AB_plot_visibility',5);
    // two input with a single int8 element
    model.in=[ones(s(2),1);-1];
    model.in2=[ones(s(2),1);-2];
    model.intyp=[ones(s(2),1);1];
    // no outputs
    model.out=[];
    model.out2=[];
    model.outtyp=[];
    
    model.blocktype='d';
    model.dep_ut=[%t %f];

    model.ipar = [herited];
    model.opar = list(names, colors);
    model.odstate = list([0;0;0]);

    model.evtin = ones(1-herited,1);

    exprs=[
           string(herited);
           '[""Corou"" ""KSC""]';
	   '[""blue"" ""green""]'
          ];

    gr_i=['txt=[''Plot_Visibility''];';
          'xstringb(orig(1),orig(2),txt,sz(1),sz(2),''fill'')'];

    x=standard_define([2 2],model,exprs,gr_i);
    graphics=x.graphics;
    graphics.style = "fillColor=white";
    x.graphics=graphics;
  end
endfunction

