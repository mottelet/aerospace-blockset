//
// This file is part of the Xcos Aerospace Blockset
//
// Copyright (C) 2013 - Pawel Zagorski
// see license.txt for more licensing informations

function [x,y,typ]=STATION_POINTING(job,arg1,arg2)
  x=[];y=[];typ=[];
  select job
   case 'plot' then
    standard_draw(arg1);
   case 'getinputs' then
    [x,y,typ]=standard_inputs(arg1);
   case 'getoutputs' then
    [x,y,typ]=standard_outputs(arg1);
   case 'getorigin' then
    [x,y]=standard_origin(arg1);
   case 'set' then
    x=arg1; //in ’set’ x is the data structure of the block
   case 'define' then
    model	= scicos_model();
    model.sim	= list('AB_gm_stationPointing',5);
    // single input with four "double" elements
    model.in	= [4; 3];
    model.in2	= [1; 1];
    model.intyp	= [1; 1];
    // one output with a single "double" element
    model.out	= [2; 1];
    model.out2	= [1; 1];
    model.outtyp= [1; 1];
    // one event output
    model.evtout = [];
    // block parameters
    model.ipar	= [];
    model.rpar	= [];
    model.opar	= list();    

    model.blocktype='c';
    model.dep_ut=[%t %f];

    exprs=string([]);
    gr_i=['txt=[''STATION_POINTING''];';
          'xstringb(orig(1),orig(2),txt,sz(1),sz(2),''fill'')'];

    x=standard_define([2 2],model,exprs,gr_i);
    graphics=x.graphics;
    graphics.style = "fillColor=white";
    x.graphics=graphics;
  end
endfunction

