//
//  Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
//  Copyright (C) 2012 - Pawel Zagorski
//
//  This file must be used under the terms of the CeCILL.
//  This source file is licensed as described in the file COPYING, which
//  you should have received as part of this distribution.  The terms
//  are also available at
//  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function block=AB_ex_secularJ2(block,flag)

previousprot = funcprot(0)

block_name = "AB_ex_secularJ2";
block_label = "SECULAR_J2";
//xcos_block_debug(block, flag, block_name);
if scicos_debug() == 2 then xcos_block_debug(block, flag, block_name); end

select flag
  case 1 then
    // set propagation parameters
    t1 = block.inptr(2);
    t2 = block.inptr(3);
    kep_t1 = block.inptr(1);
    time = getscicosvars("t0");
    // Output computation
    err = AB_check_kep(kep_t1, time, block_label);
    if err < 0 then
	set_blockerror(err);
    elseif err > 0 then
        // propagate
        [kep_t2] = CL_ex_secularJ2(t1,kep_t1,t2);
        //update outputs
        block.outptr(1) = kep_t2;
    end
  case 2 then
    // Discrete state computation
    ;
  case 4 then
    // Initialization
    AB_check_param_number(block_name, block, 0, 0, 0);
  case 5 then
    // Ending
    ;
  case 6 then
    ;
  else
    error(msprintf(gettext("%s : Incorect flag %d"), block_name, flag));
end

if scicos_debug() == 2 then xcos_block_perf(); end
//xcos_block_perf();
funcprot(previousprot);

endfunction
