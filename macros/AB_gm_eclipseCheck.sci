//
//  Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
//  Copyright (C) 2013 - Pawel Zagorski
//
//  This file must be used under the terms of the CeCILL.
//  This source file is licensed as described in the file COPYING, which
//  you should have received as part of this distribution.  The terms
//  are also available at
//  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function block=AB_gm_eclipseCheck(block,flag)

previousprot = funcprot(0)

block_name = "AB_gm_eclipseCheck";
block_label = "ECLIPSE_MODEL";
//xcos_block_debug(block, flag, block_name);
if scicos_debug() == 2 then xcos_block_debug(block, flag, block_name); end

select flag
  case 1 then
  // Output computation
    radius_bright = block.opar(1);
    radius_dark = block.opar(2);
    pos_obs = block.inptr(1);
    pos_bright = block.inptr(2);
    pos_dark = block.inptr(3);
    bodies_distance = CL_norm(pos_bright - pos_dark);
    time = getscicosvars("t0");

    if (radius_bright + radius_dark > bodies_distance) then
	warning(msprintf(gettext("\t%s\t: Eclipsed and eclipsing body collide with each other at time = %f."), block_label, time));	
    end
    if (radius_dark <= 0) then
	warning(msprintf(gettext("\t%s\t: Radius of eclipsing body is not greater than zero."), block_label));	
    end
    if (radius_bright <= 0) then
	warning(msprintf(gettext("\t%s\t: Radius of eclipsed body is not greater than zero."), block_label));	
    end

    try
        output = CL_gm_eclipseCheck(pos_obs, pos_bright, pos_dark, radius_bright, radius_dark);
        block.outptr(1) = output;		// Hidden fraction of a body
        block.outptr(2) = 1 - output;		// Visible fraction of a body
    catch
	obs_dark_distance = CL_norm(pos_obs - pos_dark);
	obs_bright_distance = CL_norm(pos_obs - pos_bright);
	if (radius_dark > obs_dark_distance) then
		warning(msprintf(gettext("\t%s\t: Observer is located within the eclipsing body at time = %f."), block_label, time));	
	end
	if (radius_bright > obs_bright_distance) then
		warning(msprintf(gettext("\t%s\t: Observer is located within the eclipsed body at time  = %f."), block_label, time));	
	end
	set_blockerror(-1)
   	error(msprintf(gettext("\t%s\t: Could not execute CL_gm_eclipseCheck() properly."), block_label));
    end
  case 4 then
  // Initialization
    AB_check_param_number(block_name, block, 0, 1, 0);
    radius_bright = block.opar(1);
    radius_dark = block.opar(2);
  case 5 then
  // Ending
    ;
  case 6 then
  // Initialization, fixed-point computation
    ;
  else
    error(msprintf(gettext("%s : Incorect flag %d"), block_name, flag));
end

if scicos_debug() == 2 then xcos_block_perf(); end
//xcos_block_perf();
funcprot(previousprot);

endfunction
