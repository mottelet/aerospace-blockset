//
//  Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
//  Copyright (C) 2012 - Pawel Zagorski
//
//  This file must be used under the terms of the CeCILL.
//  This source file is licensed as described in the file COPYING, which
//  you should have received as part of this distribution.  The terms
//  are also available at
//  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function block=AB_plot_groundtrack(block,flag)

previousprot = funcprot(0)

block_name = "AB_plot_groundtrack";
//xcos_block_debug(block, flag, block_name);
if scicos_debug() == 2 then xcos_block_debug(block, flag, block_name); end

select flag
  case 1 then
  // Output computation
    vec = block.inptr(1);
    if ~and(vec == zeros(3,1)) then
      vec = CL_co_car2sph(vec);
      vec = CL_rad2deg(vec);
      win_id = findobj("Tag", block.uid);
      [color_id traj_color_id thickness traj_thickness steps data_bounds res coord]=AB__get_pg_params(block);
      //%CL_rad2deg = 180/%pi; //defining myself because it is faster
      //CL_init();
      //CL_plot_ephem([win_id.user_data vec], win_id, traj_color_id, [], ..
      //  area, traj_thickness);l
      AB__plot_ephem(win_id, [win_id.user_data vec], traj_color_id, traj_thickness, data_bounds);
      win_id.user_data = vec;
    else
      time = getscicosvars("t0");
      if time <> 0 then
        warning(msprintf("\t%s : Encountered empty input vector at time = %f!\n", block_name, time));
      end
    end
  case 4 then
  // Initialization
    AB_check_param_number(block_name, block, 5, 1, 0);
    // Figure needs to be created or reused before any color() call to 
    // avoid creating empty graphic window
    win_id = findobj("Tag", block.uid);
    if win_id == [] then			// create new plot
      win_id = figure("Tag", block.uid,'Figure_name', ..
        'Groundtrack plot');
      win_id.background = color("white");
    else					// or reset the figure
      scf(win_id);
      clf();
    end         
    [color_id traj_color_id thickness traj_thickness steps area res coord]=AB__get_pg_params(block);
    axes_id = win_id.children;
    xlabel(axes_id, "longitude [degrees]");
    ylabel(axes_id, "latitude [degrees]");
    AB__plot_earthMap(win_id,color_id,steps,area,thickness,res,coord);
  case 5 then
  // Ending
    win_id = findobj("Tag", block.uid);
    win_id.user_data = [];
  case 6 then
  // Initialization, fixed-point computation
    ;
  else
    error(msprintf(gettext("%s : Incorect flag %d"), block_name, flag));
end

if scicos_debug() == 2 then xcos_block_perf(); end
//xcos_block_perf();
funcprot(previousprot);

endfunction
