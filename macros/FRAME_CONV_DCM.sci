//
// This file is part of the Xcos Aerospace Blockset
//
// Copyright (C) 2014 - Pawel Zagorski
// see license.txt for more licensing informations

function [x,y,typ]=FRAME_CONV_DCM(job,arg1,arg2)
  x=[];y=[];typ=[];
  select job
   case 'plot' then
    standard_draw(arg1);
   case 'getinputs' then
    [x,y,typ]=standard_inputs(arg1);
   case 'getoutputs' then
    [x,y,typ]=standard_outputs(arg1);
   case 'getorigin' then
    [x,y]=standard_origin(arg1);
   case 'set' then
    x=arg1; //in ’set’ x is the data structure of the block
    graphics=arg1.graphics;
    exprs=graphics.exprs;
    model=arg1.model;

    while %t do
      labels = ["Input reference frame"; "Output reference frame"; "UT1-TREF [seconds]"; "TT-TREF [seconds]"; "Position of the pole in ITRS [rad]"; "Corrections to CIP coordinates [rad]"];
      types = list('str', 1, 'str', 1, 'col', 1, 'col', 1, 'row', 2, 'row', 2);
      [ok, frame_IN, frame_OUT, ut1_tref, tt_tref, pcor, cipcor, exprs]=getvalue(["Set FRAME_CONV_DCM block parameters"; "Possible reference frames are:"; "ECI, ECF, ITRS, TIRS, PEF, Veis, CIRS, TOD, TEME, GCRS, MOD, EME2000, EOD"],labels, types, exprs);

      if ~ok then break,end

      //check validity of parameters
      if and(frame_IN <> ["ECI", "ECF", "ITRS", "TIRS", "PEF", "Veis", "CIRS", "TOD", "TEME", "GCRS", "MOD", "EME2000", "EOD"]) then
	block_parameter_error(msprintf(gettext("Wrong value for ''%s'' parameter"), gettext("Inpute reference frame")), ..
          gettext("Value must be a valid reference frame name (ECI, ECF, ITRS, TIRS, PEF, Veis, CIRS, TOD, TEME, GCRS, MOD, EME2000, EOD)."));
        ok=%f;
      end 
      if and(frame_OUT <> ["ECI", "ECF", "ITRS", "TIRS", "PEF", "Veis", "CIRS", "TOD", "TEME", "GCRS", "MOD", "EME2000", "EOD"]) then
        block_parameter_error(msprintf(gettext("Wrong value for ''%s'' parameter"), gettext("Output reference frame")), ..
          gettext("Value must be a valid reference frame name (ECI, ECF, ITRS, TIRS, PEF, Veis, CIRS, TOD, TEME, GCRS, MOD, EME2000, EOD)."));
        ok=%f;
      end 
      if frame_IN == frame_OUT then
        block_parameter_error(msprintf(gettext("Input and output reference frame must be different.")), "");
        ok=%f;
      end 
      //...
      //[model,graphics,ok]=set_io(model,graphics,in,out,...
      //clkin,clkout,in_implicit,out_implicit)
      //...
      if ok then
        //model.ipar	= [];
        model.rpar	= [ut1_tref, tt_tref];
        model.opar	= list(frame_IN, frame_OUT, pcor, cipcor);
        graphics.exprs=exprs;
	//graphics = AB_set_block_style(graphics, 'default');
	graphics.id=frame_IN+"->"+frame_OUT;
        x.graphics=graphics;
        x.model=model
        break
      else
        message("Failed to update block io");
      end
    end
   case 'define' then
    frame_IN = "ECI";
    frame_OUT = "ECF";
    ut1_tref = CL_dataGet("UT1_TREF");
    tt_terf = CL_dataGet("TT_TREF");
    xp = 0;
    yp = 0;
    dX = 0;
    dY = 0;

    model	= scicos_model();
    model.sim	= list('AB_fr_convertMat',5);
    // two inputs with a single "double" element
    model.in	= [1];
    model.in2	= [1];
    model.intyp	= [1];
    // one output with a single "double" element
    model.out	= [3;3];
    model.out2	= [3;1];
    model.outtyp= [1;1];
    // block parameters
    model.ipar	= [];
    model.rpar	= [ut1_tref, tt_terf];
    model.opar = list(frame_IN, frame_OUT, [xp,yp], [dX,dY]);

    model.blocktype='c';
    model.dep_ut=[%t %f];

    exprs=[	frame_IN;
		frame_OUT;
		string(ut1_tref);
		string(tt_terf);
		"[0,0]";
		"[0,0]";
	  ];

    gr_i=['txt=[''FRAME_CONV_DCM''];';
          'xstringb(orig(1),orig(2),txt,sz(1),sz(2),''fill'')'];
    //gr_i=['txt=[''Frame conversion''];';
    //      'xstringb(orig(1),orig(2),txt,sz(1),sz(2),''fill'')'];

    x=standard_define([2 4],model,exprs,gr_i);
    graphics=x.graphics;
    graphics.style = "fillColor=white";
    graphics.id=frame_IN+"->"+frame_OUT;
    //graphics.in_label=["xyz";"V";"cjd"];
    //graphics.out_label=["xyz";"V"];
    x.graphics=graphics;
  end
endfunction

