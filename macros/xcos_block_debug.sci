//
//  Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
//  Copyright (C) 2012 - Pawel Zagorski
//
//  This file must be used under the terms of the CeCILL.
//  This source file is licensed as described in the file COPYING, which
//  you should have received as part of this distribution.  The terms
//  are also available at
//  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt



function []=xcos_block_debug(block, flag, block_name)

if or(scicos_debug() == [2 3]) then
  select flag
    case 0 then 
      mprintf(">>>> %s : Continuous state derivative computation (flag==0)\n", block_name); 
      mprintf(">>>> (type: %d   uid:%s)\n", block.type, block.uid);
      mprintf(">>>> (nmode: %d)\n", block.nmode);
      mprintf(">>>>\n");
    case 1 then
      mprintf(">>>> %s : Output computation (flag==1)\n", block_name);
      mprintf(">>>> (type: %d   uid:%s)\n", block.type, block.uid);
      mprintf(">>>> (nmode: %d)\n", block.nmode);
      mprintf(">>>>\n");
    case 2 then
      mprintf(">>>> %s : Discrete state computation (flag==2)\n", block_name);
      mprintf(">>>> (type: %d   uid:%s)\n", block.type, block.uid);
      mprintf(">>>> (nmode: %d)\n", block.nmode);
      mprintf(">>>>\n");
    case 3 then
      mprintf(">>>> %s : Output event computation (flag==3)\n", block_name);
      mprintf(">>>> (type: %d   uid:%s)\n", block.type, block.uid);
      mprintf(">>>> (nmode: %d)\n", block.nmode);
      mprintf(">>>>\n");
    case 4 then
      mprintf(">>>> %s : Initialization (flag==4)\n", block_name);
      mprintf(">>>> (type: %d   uid:%s)\n", block.type, block.uid);
      mprintf(">>>> (nmode: %d)\n", block.nmode);
      mprintf("> Inputs: %d\n", block.nin);
      //mprintf("> Event inputs: %d\n", block.nevin);
      mprintf("> Outputs: %d", block.nout);
      mprintf("\tEvent outputs: %d\n", block.nevout);
      mprintf("> Parameters(integer): %d", block.nipar);
      mprintf("\tParameters(real): %d", block.nrpar);
      mprintf("\tParameters(object): %d\n", block.nopar);
      mprintf("> States -> Discrete(real): %d", block.nz);
      mprintf("\tDiscrete(object): %d", block.noz);
      mprintf("\tContinuous(real): %d\n", block.nx);
      //mprintf("\tnin = %d \tnout = %d\tnipar = %d\tnrpar = %d\tnopar = %d\n", block.nin, block.nout, block.nipar, block.nrpar, block.nopar);
      //mprintf("> nz = %d  \tnoz = %d \tnx = %d  \tnevout = %d\ttype = %d\n", block.nz, block.noz, block.nx, block.nevout, block.type);
       mprintf(">>>>\n");
    case 5 then
      mprintf(">>>> %s : Ending (flag==5)\n", block_name);
      mprintf(">>>> (type: %d   uid:%s)\n", block.type, block.uid);
      mprintf(">>>> (nmode: %d)\n", block.nmode);
      mprintf(">>>>\n");
    case 6 then
      mprintf(">>>> %s : Initialization, fixed-point computation (flag==6)\n", block_name);
      mprintf(">>>> (type: %d   uid:%s)\n", block.type, block.uid);
      mprintf(">>>> (nmode: %d)\n", block.nmode);
      mprintf(">>>>\n");
    case 7 then
      mprintf(">>>> %s : Properties of the continuous state variables (flag==7)\n", block_name);
      mprintf(">>>> (type: %d   uid:%s)\n", block.type, block.uid);
      mprintf(">>>> (nmode: %d)\n", block.nmode);
      mprintf(">>>>\n");
    case 8 then
      mprintf(">>>> %s : Initialization, fixed-point computation (flag==8)\n", block_name);
      mprintf(">>>> (type: %d   uid:%s)\n", block.type, block.uid);
      mprintf(">>>> (nmode: %d)\n", block.nmode);
      mprintf(">>>>\n");
    case 9 then
      mprintf(">>>> %s : Modes and zero crossing computation (flag==9)\n", block_name);
      mprintf(">>>> (type: %d   uid:%s)\n", block.type, block.uid);
      mprintf(">>>> (nmode: %d)\n", block.nmode);
      mprintf(">>>>\n");
    case 10 then
      mprintf(">>>> %s : Jacobian computation (Modelica) (flag==10)\n", block_name);
      mprintf(">>>> (type: %d   uid:%s)\n", block.type, block.uid);
      mprintf(">>>> (nmode: %d)\n", block.nmode);
      mprintf(">>>>\n");
  end
  
  tic();      // Used to measure performance by function xcos_block_perf()
end


endfunction
