// Copyright (C) 2010 - DIGITEO - Clément DAVID
//
// This file is released under the 3-clause BSD license. See COPYING-BSD.

function subdemolist = demo_gateway()

  demopath = get_absolute_file_path("aerospace_blockset.dem.gateway.sce");
  subdemolist = [_("Satellite on orbit"), "satellite_on_orbit.sce",
		 _("Ground station accessibility"), "ground_station_accessibility.sce",
		 _("Ground station monitor"), "ground_station_monitor.sce",
		 _("Weather ballon descent with parachute"), "weather_balloon_parachute.sce",
		 _("Sun and moon visibility from Earth location"), "sun_and_moon.sce",
		 _("Inner planets trajectories"), "inner_planets.sce",
         _("World clock"), "world_clock.sce",
         _("Quadrocopter attitude estimation with TRIAD"), "uav_triad.sce"
                ]; // add demos here
  subdemolist(:,2) = demopath + subdemolist(:,2);
 
endfunction

subdemolist = demo_gateway();
clear demo_gateway; // remove demo_gateway on stack
