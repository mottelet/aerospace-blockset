changelog of the Xcos Aerospace Blockset toolbox

aerospace_blockset (3.0)
    * Mixed bugfix/feature release.
 -- Paweł Zagórski <pzagor@agh.edu.pl> Tue, 12 Oct 2014 12:15:00 +0100
 
     - updated "Satellite on orbit" demo to showcase additional functionalities
     - added ELL_TO_CAR and CAR_TO_ELL blocks
     - added QUAT_TO_AXA, AXA_TO_QUAT, DCM_TO_QUAT and QUAT_TO_DCM blocks
     - added "World clock" demo
     - added "Quadrocopter attitude estimation with TRIAD" demo
     - added TIMEZONE_CONVERSION block
     - added FRAME_CONV_DCM block
     - added CJD_NOW block


aerospace_blockset (2.1)
    * Mixed bugfix/maintanance release.
 -- Paweł Zagórski <pzagor@agh.edu.pl> Tue, 12 Dec 2014 19:05:00 +0100
 
     - fixed problems arising after block.label property was removed in Scilab 5.5.0
     - added xcos_block_perf() function to aid with blockset profiling
     - fixed ROTATION_JACOBIAN block to only accept normalized inputs and return error otherwise
     - huge improvement of PLOT_INDICATORS block performance (typically 2.5x times fatster)
     - improvement of PLOT_VISIBILITY block performance (typically execution time cut by 25%)
     - fixed bug in PLOT_GRONDTRACK block. Now it is around 20x (twenty times!) faster
     - FRAME_CONVERSION block execution times cut by 20%
     - US76_ATM_MODEL block execution times cut by more than 30%
     - Addded direct links to demonstrations in blockset help
     - Improved performance in diagrams which require small sampling times
     - CJD_TIME block performance upgrade (3-4 times faster)
     - Improved performance of DISTANCE_CONVERSION, ANGLE_CONVERSION, and VELOCITY_CONVERSION blocks (about 3x faster)

aerospace_blockset (2.0)
    * Mixed bugfix/feature release.
 -- Paweł Zagórski <pzagor@agh.edu.pl> Tue, 15 Dec 2013 19:05:00 +0100

    - added blocks for vector computations: DOT_PRODUCT, CROSS_PRODUCT, VECTOR_NORM
    - added blocks for quaternion computations (eg. attitude dynamics): QUAT_CONJUGATE, QUAT_INPUT, VECTOR_ROTATE, ROTATION_JACOBIAN, QUAT_NORM, QUAT_INVERT, QUAT_PRODUCT, QUAT_QUOTIENT
    - added environmental model blocks: GEOMAG_MODEL, ECLIPSE_MODEL, ATM_DRAG_MODEL, SOL_DRAG_MODEL, PLANET_MODEL
    - added UI plotting blocks: PLOT_POLAR, PLOT_INDICATORS
    - moved all examples and demonstrations to new *.zcos format for performance
    - removed ECI_TO_ECF block and substituted it wuith FRAME_CONVERSION block, that is able to convert between numerous reference frames
    - added FRAME_CONV_JACOBIAN block
    - added STATION_POINTING block
    - added 2 new demonstrations: "Ground station monitor", "Inner planets trajectories"
    - modified blocks to use new icon styling by removing the ugly gray gradient background
    - numerous bugfixes
    - improved performance of many blocks, mostly in graphic departament

aerospace_blockset (1.2)
    * Bugfix release.
 -- Paweł Zagórski <pzagor@agh.edu.pl> Tue, 28 Aug 2013 14:11 +0100

    - Removed block G50_TO_TER and added ECI_TO_ECF instead to account for reference frames hanbling in CelestLab 3.0
    - Updated unit tests to take into account new handling of reference frames in CelestLab 3.0
    - Updated unit tests to take into account changes in binary format in Scilab 5.4
    - Modified multiple blocks not to use functions that became deprecated in CelestLab 3.0
    - Fixed typos in VECTOR_ANGLE and VECTOR_NORMALIZATION help file names.
    - Added developer script allowing to check if all blocks have coresponding help files
    - VECTOR_NORMAALIZATION block now does not display incorrect warning about empty input vectors
    - VECTOR_NORMAALIZATION block icon was updated to use correct mathematic simbolic (previously symbol was suggesting vector norm)
    - computational function were modified not to display unnecessary warning about function redefinitins

aerospace_blockset (1.1)
    * Mixed bugfix/feature release.
 -- Paweł Zagórski <pzagor@agh.edu.pl> Tue, 30 Aug 2012 18:15:00 +0100

    - Added VECTOR_ANGLE block
    - Added VECTOR_NORMALIZATION block
    - Added LYDDANE_PROPAGATOR block
    - Added KEPLERIAN_PROPAGATOR block
    - Improved performance of PLOT_GROUNDTRACK block
    - Improved performance of PLOT_VISIBILITY block
    - Made sure PLOT_VISIBILITY block refereshes at least once every 3s to avoid apparent freezes
    - Improved help pages (divided up into sections, added overview section)
    - fixed warning message shown when CelestLab is not installed to suggest user ways to solve the problem.
    - fixed US76_atm test (it was to liberal)
    - fixed a bug in all blocks that prevented warning message to be displayed in case of incorrect number of object parameters passed to computational functions
    - minor fixes for DISTANCE_CONVERSION block unit tests
    - modified "Ground station accessibility" to use Lyddane propagator
    - removed unnecessary c builder filesto allow building blockset without c compiler
    - complettely removed DUMMY_BLOCK (needed only for debugging purposes) from the release version


aerospace_blockset (1.0)
    * Set of inital aerospace blockset blocks and features.
 -- Paweł Zagórski <pzagor@agh.edu.pl> Tue, 8 Aug 2012 12:32:36 +0100

    - Added ANGLE_CONVERSION block
    - Added CJD_TIME block
    - Added DISTANCE_CONVERSION block
    - Added G50_TO_TER block
    - Added GROUND_STATION block
    - Added KEPLERIAN_INPUT block
    - Added KEP_TO_CAR block
    - Added MOON_MODEL block
    - Added PLOT_GROUNDTRACK block
    - Added PLOT_VISIBILITY block
    - Added SECULAR_J2 block
    - Added STATION_VISIBILITY block
    - Added SUN_MODEL block
    - Added TIMEFRAME_CONVERSION block
    - Added US76_ATM_MODEL block
    - Added VELOCITY_CONVERSION block
