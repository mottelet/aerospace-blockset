<?xml version="1.0" encoding="UTF-8"?>
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" xmlns:ns5="http://www.w3.org/1999/xhtml" xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" version="5.0-subset Scilab" xml:id="PLOT_GROUNDTRACK">
  <refnamediv>
    <refname>PLOT_GROUNDTRACK</refname>
    <refpurpose>Plots ground track of Earth's satellite.</refpurpose>
  </refnamediv>
  <refsection>
    <title>Block Screenshot</title>
    <para>
      <inlinemediaobject>
        <imageobject>
          <imagedata align="center" fileref="../../../images/gif/PLOT_GROUNDTRACK.gif" valign="middle"/>
        </imageobject>
      </inlinemediaobject>
    </para>
  </refsection>
  <refsection id="Contents_PLOT_GROUNDTRACK">
    <title>Contents</title>
    <itemizedlist>
      <listitem>
        <para>
          <link linkend="PLOT_GROUNDTRACK">PLOT_GROUNDTRACK block</link>
        </para>
      </listitem>
      <listitem>
        <itemizedlist>
          <listitem>
            <para>
              <xref linkend="Description_PLOT_GROUNDTRACK">Description</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Data_types_PLOT_GROUNDTRACK">Data types</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Dialogbox_PLOT_GROUNDTRACK">Dialog box</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Properties_PLOT_GROUNDTRACK">Default properties</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Example_PLOT_GROUNDTRACK">Example</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Interfacingfunction_PLOT_GROUNDTRACK">Interfacing function</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Computationalfunction_PLOT_GROUNDTRACK">Computational function</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="SeeAlso_PLOT_GROUNDTRACK">See also</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Author_PLOT_GROUNDTRACK">Author</xref>
            </para>
          </listitem>
        </itemizedlist>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Description_PLOT_GROUNDTRACK">
    <title>Description</title>
    <para>Plots contour map of specified region of Earth and shows satellite groundtrack on top of it. Trajectory must be given in cartesian coordinates. </para>
  </refsection>
  <refsection id="Data_types_PLOT_GROUNDTRACK">
    <title>Data types</title>
    <itemizedlist>
      <listitem>
        <para>Input: 3x1 double</para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Dialogbox_PLOT_GROUNDTRACK">
    <title>Dialog box</title>
<imagedata align="center" fileref="../../../images/gif/PLOT_GROUNDTRACK_dialog.gif" valign="middle"/>
    <para>Accept inherited events - if "0" block will have event input, if "1" it will refresh on inputs change</para>
    <para>Map color - color of the contour map (accepts every color supported by color() function)</para>
    <para>Map line thickness - thickness of the contour map</para>
    <para>Trajectory color - color of the trajectory line (accepts every color supported by color() function)</para>
    <para>Trajectory line thickness - thickness of the trajectory line in pixels. Must be positive.</para>
    <para>Grid size - size of the map grid. Both values must be greater than 0.</para>
    <para>Definition of the view area - bonds defining map boundaries (in degrees)</para>
    <para>Resolution - resolution of the contour map ("low" or "high")</para>
    <para>Coordinates - use spherical or elliptical coordinates ("ell" or "sph")</para>
  </refsection>
  <refsection id="Properties_PLOT_GROUNDTRACK">
    <title>Default properties</title>
    <para>Accept inherited events = 1</para>
    <para>Map color = blue</para>
    <para>Map line thickness = 1</para>
    <para>Trajectory color = red</para>
    <para>Trajectory line thickness = 2</para>
    <para>Grid size = [30,15]</para>
    <para>Definition of the view area = [-180,-90,180,90]</para>
    <para>Resolution = low</para>
    <para>Coordinates = sph</para>
  </refsection>
  <refsection id="Example_PLOT_GROUNDTRACK">
    <title>Example</title>
    <imagedata align="center" fileref="../../../images/gif/PLOT_GROUNDTRACK_example.gif" valign="middle"/>
    <para>This example can be found under: {Aerospace Toolbox}/help/en_US/plot_groundtrack.zcos</para>
  </refsection>
  <refsection id="Interfacingfunction_PLOT_GROUNDTRACK">
    <title>Interfacing function</title>
    <itemizedlist>
      <listitem>
        <para>macros/PLOT_GROUNDTRACK.sci</para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Computationalfunction_PLOT_GROUNDTRACK">
    <title>Computational function</title>
    <itemizedlist>
      <listitem>
        <para>macros/AB_plot_groundtrack.sci</para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="SeeAlso_PLOT_GROUNDTRACK">
    <title>See Also</title>
    <simplelist type="inline">
<member><para><link type="scilab" linkend="scilab.help/CelestLab">CelestLab</link> - CelestLab aerospace library</para></member>
      <member><para><link type="scilab" linkend="scilab.help/CL_plot_earthMap">CL_plot_earthMap()</link> - CelestLab function used to plot Eatrh map as a background</para></member>
      <member><para><link type="scilab" linkend="scilab.help/CL_plot_ephem">CL_plot_ephem()</link> - CelestLab function used to plot the satellite trajectory on the longitude/latitude plane</para></member>
<member><para><link type="scilab" linkend="scilab.help/color">color()</link> - Function used to process user input of color parameters</para></member>
      <member><para><link type="scilab" linkend="scilab.help/PLOT_VISIBILITY">PLOT_VISIBILITY</link> - Aerospace Blockset block able to plot access diagram of ground station to satellite visibility.</para></member>
    </simplelist>
  </refsection>
  <refsection id="Author_PLOT_GROUNDTRACK">
    <title>Author</title>
    <simplelist type="inline">
      <member>Paweł Zagórski</member>
    </simplelist>
  </refsection>
</refentry>
