<?xml version="1.0" encoding="UTF-8"?>
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" xmlns:ns5="http://www.w3.org/1999/xhtml" xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" version="5.0-subset Scilab" xml:id="QUAT_CONJUGATE">
  <refnamediv>
    <refname>QUAT_CONJUGATE</refname>
    <refpurpose>This Xcos block allows to calculate conjugate of the given quaternion.</refpurpose>
  </refnamediv>
  <refsection>
    <title>Block Screenshot</title>
    <para>
      <inlinemediaobject>
        <imageobject>
          <imagedata align="center" fileref="../../../images/gif/QUAT_CONJUGATE.gif" valign="middle"/>
        </imageobject>
      </inlinemediaobject>
    </para>
  </refsection>
  <refsection id="Contents_QUAT_CONJUGATE">
    <title>Contents</title>
    <itemizedlist>
      <listitem>
        <para>
          <link linkend="QUAT_CONJUGATE">QUAT_CONJUGATE block</link>
        </para>
      </listitem>
      <listitem>
        <itemizedlist>
          <listitem>
            <para>
              <xref linkend="Description_QUAT_CONJUGATE">Description</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Data_types_QUAT_CONJUGATE">Data types</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Dialogbox_QUAT_CONJUGATE">Dialog box</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Properties_QUAT_CONJUGATE">Default properties</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Interfacingfunction_QUAT_CONJUGATE">Interfacing function</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Computationalfunction_QUAT_CONJUGATE">Computational function</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="SeeAlso_QUAT_CONJUGATE">See also</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Author_QUAT_CONJUGATE">Author</xref>
            </para>
          </listitem>
        </itemizedlist>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Description_QUAT_CONJUGATE">
    <title>Description</title>
    <para>This Xcos block allows to calculate conjugate of the given quaternion. Input quaternion is given as 4x1 vector [r; i; j; k], where r is the scalar parameter, and [i; j; k] are the imaginary vector parameters. Output quaternion is a conjugate of the input, namely it is equal to [r; -i; -j; -k].</para>
  </refsection>
  <refsection id="Data_types_QUAT_CONJUGATE">
    <title>Data types</title>
    <itemizedlist>
      <listitem>
        <para>Input: 4x1 double (considered as quaternion parameters, first one being the real part, and remaining three imaginary vector part)</para>
        <para>Output: 4x1 double (considered as quaternion parameters, first one being the real part, and remaining three imaginary vector part)</para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Dialogbox_QUAT_CONJUGATE">
    <title>Dialog box</title>
    <para>This block has no dialog box.</para>
  </refsection>
  <refsection id="Properties_QUAT_CONJUGATE">
    <title>Default properties</title>
    <para>This block has no cofigurable properties.</para>
  </refsection>
  <refsection id="Interfacingfunction_QUAT_CONJUGATE">
    <title>Interfacing function</title>
    <itemizedlist>
      <listitem>
        <para>macros/QUAT_CONJUGATE.sci</para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Computationalfunction_QUAT_CONJUGATE">
    <title>Computational function</title>
    <itemizedlist>
      <listitem>
        <para>macros/AB_quatConjugate.sci</para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="SeeAlso_QUAT_CONJUGATE">
    <title>See Also</title>
    <simplelist type="inline">
      <member><link type="scilab" linkend="scilab.help/CelestLab">CelestLab</link> - Aerospace library used for quaternion computations</member>
      <member><link type="scilab" linkend="scilab.help/CL_rot_defQuat">CL_rot_defQuat</link> - CelestLab function used for defining a quaternion structure</member>
      <member><para><link type="scilab" linkend="scilab.help/QUAT_NORM">QUAT_NORM</link> - Aerospace blockset block that caluclates quaternion norm</para></member>
      <member><para><link type="scilab" linkend="scilab.help/QUAT_INPUT">QUAT_INPUT</link> - Aerospace blockset block providing convenient way for definig quaternions</para></member>
      <member><para><link type="scilab" linkend="scilab.help/QUAT_INVERT">QUAT_INVERT</link> - Aerospace blockset block that inverts the quaternion</para></member>
      <member><para><link type="scilab" linkend="scilab.help/QUAT_PRODUCT">QUAT_PRODUCT</link> - Aerospace blockset block that calculates product of quaternions</para></member>
      <member><para><link type="scilab" linkend="scilab.help/QUAT_QUOTIENT">QUAT_QUOTIENT</link> - Aerospace blockset block allowing for dividing quaternions</para></member>
    </simplelist>
  </refsection>
  <refsection id="Author_QUAT_CONJUGATE">
    <title>Author</title>
    <simplelist type="inline">
      <member>Paweł Zagórski</member>
    </simplelist>
  </refsection>
</refentry>
