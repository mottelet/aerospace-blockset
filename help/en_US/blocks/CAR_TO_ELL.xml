<?xml version="1.0" encoding="UTF-8"?>
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" xmlns:ns5="http://www.w3.org/1999/xhtml" xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" version="5.0-subset Scilab" xml:id="CAR_TO_ELL">
  <refnamediv>
    <refname>CAR_TO_ELL</refname>
    <refpurpose>This Xcos block converts position given in cartesian coordinates to elliptical coordinates.</refpurpose>
  </refnamediv>
  <refsection>
    <title>Block Screenshot</title>
    <para>
      <inlinemediaobject>
        <imageobject>
          <imagedata align="center" fileref="../../../images/gif/CAR_TO_ELL.gif" valign="middle"/>
        </imageobject>
      </inlinemediaobject>
    </para>
  </refsection>
  <refsection id="Contents_CAR_TO_ELL">
    <title>Contents</title>
    <itemizedlist>
      <listitem>
        <para>
          <link linkend="CAR_TO_ELL">CAR_TO_ELL block</link>
        </para>
      </listitem>
      <listitem>
        <itemizedlist>
          <listitem>
            <para>
              <xref linkend="Description_CAR_TO_ELL">Description</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Data_types_CAR_TO_ELL">Data types</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Dialogbox_CAR_TO_ELL">Dialog box</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Properties_CAR_TO_ELL">Default properties</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Interfacingfunction_CAR_TO_ELL">Interfacing function</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Computationalfunction_CAR_TO_ELL">Computational function</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="SeeAlso_CAR_TO_ELL">See also</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Author_CAR_TO_ELL">Author</xref>
            </para>
          </listitem>
        </itemizedlist>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Description_CAR_TO_ELL">
    <title>Description</title>
    <para>This Xcos block converts position given in cartesian ("geodetic") coordinates to elliptical coordinates. Elliptical coordinates are longitude, latitude and altitude with respect to some reference ellipsoid. The reference ellipsoid is an ellipsoid of revolution around the Z axis. It is then characterized by 2 parameters:
- semi-major axis of the ellipse obtained as the intersection of the ellipsoid with any plane containing the Z axis,
- oblateness (or flattening) of this ellipse. Additionally transformation jacobian is given as an output. Be careful that the 3rd elliptical coordinate is an altitude and not the distance to the planet center.</para>
  </refsection>
  <refsection id="Data_types_CAR_TO_ELL">
    <title>Data types</title>
    <itemizedlist>
      <listitem><para>Input 1:  3x1 double (position in the cartesian reference frame)</para></listitem>
      <listitem><para>Output 1:  3x1 double (position in the elliptical reference frame)</para></listitem>
      <listitem><para>Output 2:  3x3 double (jacobian of the transformation)</para></listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Dialogbox_CAR_TO_ELL">
    <title>Dialog box</title>
    <imagedata align="center" fileref="../../../images/gif/CAR_TO_ELL_dialog.gif" valign="middle"/>
  </refsection>
  <refsection id="Properties_CAR_TO_ELL">
    <title>Default properties</title>
    <para>Equatorial radius of reference ellipsoid- "6378136.3"" (for the Earth)</para>
    <para>Oblatness of reference ellipsoid- "0.0033528" (for the Earth)</para>
  </refsection>
  <refsection id="Interfacingfunction_CAR_TO_ELL">
    <title>Interfacing function</title>
    <itemizedlist>
      <listitem>
        <para>macros/CAR_TO_ELL.sci</para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Computationalfunction_CAR_TO_ELL">
    <title>Computational function</title>
    <itemizedlist>
      <listitem>
        <para>macros/AB_co_car2ell.sci</para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="SeeAlso_CAR_TO_ELL">
    <title>See Also</title>
    <simplelist type="inline">
<member><para><link type="scilab" linkend="scilab.help/CelestLab">CelestLab</link> - CelestLab aerospace library</para></member>
      <member><para><link type="scilab" linkend="scilab.help/CL_co_car2ell">CL_co_car2ell</link> - CelestLab function used to perform the conversion between the reference frames.</para></member>
      <member><para><link type="scilab" linkend="scilab.help/ELL_TO_CAR">ELL_O_CAR</link> - Aerospace Blockset block converting position given in elliptical coordinates to cartttesian coordinates.</para></member>
    </simplelist>
  </refsection>
  <refsection id="Author_CAR_TO_ELL">
    <title>Author</title>
    <simplelist type="inline">
      <member>Paweł Zagórski</member>
    </simplelist>
  </refsection>
</refentry>
