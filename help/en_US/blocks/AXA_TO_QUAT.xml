<?xml version="1.0" encoding="UTF-8"?>
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" xmlns:ns5="http://www.w3.org/1999/xhtml" xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" version="5.0-subset Scilab" xml:id="AXA_TO_QUAT">
  <refnamediv>
    <refname>AXA_TO_QUAT</refname>
    <refpurpose>This Xcos block allows to easily convert rotation axis and angle to the corresponding orientation quaternion. </refpurpose>
  </refnamediv>
  <refsection>
    <title>Block Screenshot</title>
    <para>
      <inlinemediaobject>
        <imageobject>
          <imagedata align="center" fileref="../../../images/gif/AXA_TO_QUAT.gif" valign="middle"/>
        </imageobject>
      </inlinemediaobject>
    </para>
  </refsection>
  <refsection id="Contents_AXA_TO_QUAT">
    <title>Contents</title>
    <itemizedlist>
      <listitem>
        <para>
          <link linkend="AXA_TO_QUAT">AXA_TO_QUAT block</link>
        </para>
      </listitem>
      <listitem>
        <itemizedlist>
          <listitem>
            <para>
              <xref linkend="Description_AXA_TO_QUAT">Description</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Data_types_AXA_TO_QUAT">Data types</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Interfacingfunction_AXA_TO_QUAT">Interfacing function</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Computationalfunction_AXA_TO_QUAT">Computational function</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="SeeAlso_AXA_TO_QUAT">See also</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Author_AXA_TO_QUAT">Author</xref>
            </para>
          </listitem>
        </itemizedlist>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Description_AXA_TO_QUAT">
    <title>Description</title>
    <para>This Xcos block allows to easily convert rotation axis and angle to orientation quaternion. First input port accepts N rotation axes represented as 3xN matrice and second 1xN angles vector (in radians). Output produces orientation quaternions represented as 4xN matrix.</para>
  </refsection>
  <refsection id="Data_types_AXA_TO_QUAT">
    <title>Data types</title>
    <itemizedlist>
      <listitem>
        <para>Input 1: Rotation axis [ux; uy; uz] (3xN double)</para>
        <para>Input 2: Rotation angle [rad] (1xN)</para>
        <para>Output 1: Orientation quaternion (4xN double)</para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Interfacingfunction_AXA_TO_QUAT">
    <title>Interfacing function</title>
    <itemizedlist>
      <listitem>
        <para>macros/AXA_TO_QUAT.sci</para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Computationalfunction_AXA_TO_QUAT">
    <title>Computational function</title>
    <itemizedlist>
      <listitem>
        <para>macros/AB_rot_axAng2quat.sci</para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="SeeAlso_AXA_TO_QUAT">
    <title>See Also</title>
    <simplelist type="inline">
      <member><link type="scilab" linkend="scilab.help/CelestLab">CelestLab</link>- Aerospace library</member>
      <member><link type="scilab" linkend="scilab.help/CL_rot_quat2axAng">CL_rot_quat2axAng</link>- CelestLab function used for quaternion to rotation axis and angle conversion</member>
      <member><para><link type="scilab" linkend="scilab.help/QUAT_CONJUGATE">QUAT_CONJUGATE</link> - Aerospace blockset block that allows to calculate conjugate of the given quaternion.</para></member>
      <member><para><link type="scilab" linkend="scilab.help/QUAT_NORM">QUAT_NORM</link> - Aerospace blockset block that caluclates quaternion norm</para></member>
      <member><para><link type="scilab" linkend="scilab.help/QUAT_INVERT">QUAT_INVERT</link> - Aerospace blockset block that inverts the quaternion</para></member>
      <member><para><link type="scilab" linkend="scilab.help/QUAT_PRODUCT">QUAT_PRODUCT</link> - Aerospace blockset block that calculates product of quaternions</para></member>
      <member><para><link type="scilab" linkend="scilab.help/QUAT_QUOTIENT">QUAT_QUOTIENT</link> - Aerospace blockset block allowing for dividing quaternions</para></member>
      <member><para><link type="scilab" linkend="scilab.help/DCM_TO_QUAT">DCM_TO_QUAT</link> - Aerospace Blockset block allowing to easily convert Direction Cosine Matrix (DCM) to orientation quaternion. It is also possible to optionally calculate the quaternion derivative providing angular velocity vector.</para></member>
      <member><para><link type="scilab" linkend="scilab.help/QUAT_TO_DCM">QUAT_TO_DCM</link> - Aerospace Blockset block allowing to easily convert orientation quaternion to Direction Cosine Matrix (DCM). It is also possible to optionally calculate the angular velocity vector providing quaternion derivative.</para></member>
      <member><para><link type="scilab" linkend="scilab.help/QUAT_TO_AXA">QUAT_TO_AXA</link> - Aerospace Blockset block allowing to easily convert orientation quaternion to rotation axis and angle.</para></member>
    </simplelist>
  </refsection>
  <refsection id="Author_AXA_TO_QUAT">
    <title>Author</title>
    <simplelist type="inline">
      <member>Paweł Zagórski</member>
    </simplelist>
  </refsection>
</refentry>
