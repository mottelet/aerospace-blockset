<?xml version="1.0" encoding="UTF-8"?>
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" xmlns:ns5="http://www.w3.org/1999/xhtml" xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" version="5.0-subset Scilab" xml:id="ECLIPSE_MODEL">
  <refnamediv>
    <refname>ECLIPSE_MODEL</refname>
    <refpurpose>This block calculates fraction of the spherical celestial body apparent surface that is visible from behind the other spherical body.</refpurpose>
  </refnamediv>
  <refsection>
    <title>Block Screenshot</title>
    <para>
      <inlinemediaobject>
        <imageobject>
          <imagedata align="center" fileref="../../../images/gif/ECLIPSE_MODEL.gif" valign="middle"/>
        </imageobject>
      </inlinemediaobject>
    </para>
  </refsection>
  <refsection id="Contents_ECLIPSE_MODEL">
    <title>Contents</title>
    <itemizedlist>
      <listitem>
        <para>
          <link linkend="ECLIPSE_MODEL">ECLIPSE_MODEL block</link>
        </para>
      </listitem>
      <listitem>
        <itemizedlist>
          <listitem>
            <para>
              <xref linkend="Description_ECLIPSE_MODEL">Description</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Data_types_ECLIPSE_MODEL">Data types</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Dialogbox_ECLIPSE_MODEL">Dialog box</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Properties_ECLIPSE_MODEL">Default properties</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Interfacingfunction_ECLIPSE_MODEL">Interfacing function</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Computationalfunction_ECLIPSE_MODEL">Computational function</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="SeeAlso_ECLIPSE_MODEL">See also</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Author_ECLIPSE_MODEL">Author</xref>
            </para>
          </listitem>
        </itemizedlist>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Description_ECLIPSE_MODEL">
    <title>Description</title>
    <para>This block calculates fraction of the spherical celestial body apparent surface that is visible from behind the other spherical body. Coordinates of the observers and positions of the two celestial bodies are given as inputs. Radii of the two bodies are assument constant with time and are configurable in block parameters dialog. Simple geometrical model is used to calculate the fraction of the body that is visible. Both eclipsed and the visible fraction of the apparent surface of a body are given as outputs in form of real numbers ranging from 0 to 1. Sum of those two always equals to one. For example: </para>
    <itemizedlist>
      <listitem>
	<para>Output 1 = 0, Output 2 = 1   - no eclipse</para>
	<para>Output 1 = 1, Output 2 = 0   - full eclipse</para>
	<para>Output 1 = 0.5, Output 2 = 0.5 - half of the body cross-section is ecplised</para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Data_types_ECLIPSE_MODEL">
    <title>Data types</title>
    <itemizedlist>
      <listitem>
        <para>Input 1: Position of the observer in Cartesian reference frame described by 3x1 vector</para>
        <para>Input 2: Position of the body that is being eclipsed in Cartesian reference frame described by 3x1 vector</para>
        <para>Input 3: Position of the body that obscures the view in Cartesian reference frame described by 3x1 vector</para>
        <para>Output 1: Fraction of the apparent surface of the body that is eclipsed (real value between 0 and 1)</para>
        <para>Output 2: Fraction of the apparent surface of the body that is visible (real value between 0 and 1)</para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Dialogbox_ECLIPSE_MODEL">
    <title>Dialog box</title>
    <imagedata align="center" fileref="../../../images/gif/ECLIPSE_MODEL_dialog.gif" valign="middle"/>
    <para>Radius of eclipsed body - Radius of the body that is being eclipsed.</para>
    <para>Radius of eclipsing body - Radius of the body that constraints the view of the other body.</para>
    <para>Accept herited events (0/1) - If 1 the block will accept herited events, if 0 it will have its own event input.</para>
  </refsection>
  <refsection id="Properties_ECLIPSE_MODEL">
    <title>Default properties</title>
    <para>Radius of eclipsed body = 6.960D+08 (radius of the Sun in kilometers)</para>
    <para>Radius of eclipsing body = 6378136.3 (radius of the Earth in kilometers)</para>
    <para>Accept herited events (0/1)= 0</para>
  </refsection>
  <refsection id="Interfacingfunction_ECLIPSE_MODEL">
    <title>Interfacing function</title>
    <itemizedlist>
      <listitem>
        <para>macros/ECLIPSE_MODEL.sci</para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Computationalfunction_ECLIPSE_MODEL">
    <title>Computational function</title>
    <itemizedlist>
      <listitem>
        <para>macros/AB_gm_eclipseCheck.sci</para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="SeeAlso_ECLIPSE_MODEL">
    <title>See Also</title>
    <simplelist type="inline">
      <member><link type="scilab" linkend="scilab.help/CelestLab">CelestLab</link>- CelestLab aerospace library</member>
      <member><link type="scilab" linkend="scilab.help/CL_gm_eclipseCheck">CL_gm_eclipseCheck</link>- CelestLab function used to evaluate eclipse conditions.</member>
      <member><link type="scilab" linkend="scilab.help/SUN_MODEL">SUN_MODEL</link>- Aerospace blockset block allowing for calculation of the Sun position.</member>
      <member><link type="scilab" linkend="scilab.help/MOON_MODEL">MOON_MODEL</link>- Aerospace blockset block allowing for calculation of the Moon position.</member>
    </simplelist>
  </refsection>
  <refsection id="Author_ECLIPSE_MODEL">
    <title>Author</title>
    <simplelist type="inline">
      <member>Paweł Zagórski</member>
    </simplelist>
  </refsection>
</refentry>
