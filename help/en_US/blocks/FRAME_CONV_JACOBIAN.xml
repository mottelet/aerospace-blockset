<?xml version="1.0" encoding="UTF-8"?>
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" xmlns:ns5="http://www.w3.org/1999/xhtml" xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" version="5.0-subset Scilab" xml:id="FRAME_CONV_JACOBIAN">
  <refnamediv>
    <refname>FRAME_CONV_JACOBIAN</refname>
    <refpurpose>This Xcos block calculates jacobian of transformation of position and velocity vectors from one reference frame to another</refpurpose>
  </refnamediv>
  <refsection>
    <title>Block Screenshot</title>
    <para>
      <inlinemediaobject>
        <imageobject>
          <imagedata align="center" fileref="../../../images/gif/FRAME_CONV_JACOBIAN.gif" valign="middle"/>
        </imageobject>
      </inlinemediaobject>
    </para>
  </refsection>
  <refsection id="Contents_FRAME_CONV_JACOBIAN">
    <title>Contents</title>
    <itemizedlist>
      <listitem>
        <para>
          <link linkend="FRAME_CONV_JACOBIAN">FRAME_CONV_JACOBIAN block</link>
        </para>
      </listitem>
      <listitem>
        <itemizedlist>
          <listitem>
            <para>
              <xref linkend="Description_FRAME_CONV_JACOBIAN">Description</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Data_types_FRAME_CONV_JACOBIAN">Data types</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Dialogbox_FRAME_CONV_JACOBIAN">Dialog box</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Properties_FRAME_CONV_JACOBIAN">Default properties</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Interfacingfunction_FRAME_CONV_JACOBIAN">Interfacing function</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Computationalfunction_FRAME_CONV_JACOBIAN">Computational function</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="SeeAlso_FRAME_CONV_JACOBIAN">See also</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Author_FRAME_CONV_JACOBIAN">Author</xref>
            </para>
          </listitem>
        </itemizedlist>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Description_FRAME_CONV_JACOBIAN">
    <title>Description</title>
    <para>This Xcos block calculates jacobian of transformation of position and velocity vectors from one reference frame to another. Available reference frames are: ECI, ECF, ITRS, TIRS, PEF, Veis, CIRS, TOD, TEME, GCRS, MOD, EME2000 and EOD. Detailed description of those reference frames can be found in <link type="scilab" linkend="scilab.help/CL_fr_convert">CL_fr_convert</link> help chapter. First two inputs accept 3x1 vectors of position and velocity. Third input with current simulation time in "modified julian day since 1950" needs to be always connected, because reference frame conversions rely on current time.</para>
  </refsection>
  <refsection id="Data_types_FRAME_CONV_JACOBIAN">
    <title>Data types</title>
    <itemizedlist>
      <listitem><para>Input 1 (mandatory):  3x1 double (position in the input reference frame)</para></listitem>
      <listitem><para>Input 2 (optional):  3x1 double (velocity in the input reference frame)</para></listitem>
      <listitem><para>Input 3 (mandatory):  1x1 double (time in "modified julian day since 1950" format)</para></listitem>
      <listitem><para>Output 1:  6x1 double (jacobian of the transformation)</para></listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Dialogbox_FRAME_CONV_JACOBIAN">
    <title>Dialog box</title>
    <imagedata align="center" fileref="../../../images/gif/FRAME_CONV_JACOBIAN_dialog.gif" valign="middle"/>
    <para>Input reference frame- reference frame of the input vetors</para>
    <para>Output reference frame- reference frame of the output vetors</para>
    <para>UTI-TREF- UTI-TREF [s] (1xN or 1x1)</para>
    <para>TT-TREF- TT-TREF [s] (1xN or 1x1)</para>
    <para>Position of the pole in ITRS- position of the pole [rad] (2xN or 2x1)</para>
    <para>Correction to CIP coordinates- position of the pole [rad] (1x2)</para>
  </refsection>
  <refsection id="Properties_FRAME_CONV_JACOBIAN">
    <title>Default properties</title>
    <para>Input reference frame- "ECI"</para>
    <para>Output reference frame- "ECF" "</para>
    <para>UTI-TREF- 0</para>
    <para>TT-TREF- 67.184</para>
    <para>Position of the pole in ITRS- [0,0]</para>
    <para>Correction to CIP coordinates- [0,0]</para>
  </refsection>
  <refsection id="Interfacingfunction_FRAME_CONV_JACOBIAN">
    <title>Interfacing function</title>
    <itemizedlist>
      <listitem>
        <para>macros/FRAME_CONV_JACOBIAN.sci</para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Computationalfunction_FRAME_CONV_JACOBIAN">
    <title>Computational function</title>
    <itemizedlist>
      <listitem>
        <para>macros/AB_fr_convert.sci</para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="SeeAlso_FRAME_CONV_JACOBIAN">
    <title>See Also</title>
    <simplelist type="inline">
<member><para><link type="scilab" linkend="scilab.help/CelestLab">CelestLab</link> - CelestLab aerospace library</para></member>
      <member><para><link type="scilab" linkend="scilab.help/CL_fr_convert">CL_fr_convert</link> - CelestLab function used to perform the conversion between the reference frames.</para></member>
      <member><para><link type="scilab" linkend="scilab.help/KEP_TO_CAR">KEP_TO_CAR</link> - Aerospace Blockset block allowing for conversion from classical keplerian orbital elements to position and velocity for an elliptic, hyperbolic or parabolic orbit</para></member>
      <member><para><link type="scilab" linkend="scilab.help/CJD_TIME">CJD_TIME</link> - Aerospace Blockset block providing configurable way of feeding time to the simulation in "modified julian day since 1950" format.</para></member>
      <member><para><link type="scilab" linkend="scilab.help/PLOT_GROUNDTRACK">PLOT_GROUNDTRACK</link> - Aerospace Blockset block allowing to plot ground track of satellites orbiting Earth based on their position in terrestial reference frame.</para></member>
      <member><para><link type="scilab" linkend="scilab.help/FRAME_CONVERSION">FRAME_CONVERSION</link> - Aerospace Blockset block allowing for conversion of position and velocity vectors from one reference frame to another.</para></member>
      <member><para><link type="scilab" linkend="scilab.help/FRAME_CONV_DCM">FRAME_CONV_DCM</link> - Aerospace Blockset block allowing to calculate frame transformation matrix and angular velocity vector of transformation between reference frames</para></member>
    </simplelist>
  </refsection>
  <refsection id="Author_FRAME_CONV_JACOBIAN">
    <title>Author</title>
    <simplelist type="inline">
      <member>Paweł Zagórski</member>
    </simplelist>
  </refsection>
</refentry>
